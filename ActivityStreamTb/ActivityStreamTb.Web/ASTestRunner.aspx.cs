﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActivityStreamTb.Web
{
    public delegate void LoadDumpDelegate(string results);

    public partial class ASTestRunner : System.Web.UI.Page
    {        
        LoadDumpDelegate DumpMessage;
        LoadDumpDelegate DumpResults;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(RunButton);
            }
            catch { }
            RunButton.Attributes.Add("onclick", string.Format("window.open('{0}', '', '', 'height=200,width=400'", ASConfig.DummyStore));
            DumpMessage = new LoadDumpDelegate(_dump_message);
            DumpResults = new LoadDumpDelegate(_dump_results);
            DumpTextbox.Text = "";
            ResultsTextbox.Text = "";
            //StoreIdTextBox.Text = ASConfig.TestSingleStoreId;
            //StoreDbTextBox.Text = ASConfig.TestSingleStoreDb;
        }

        protected void RunButton_Click(object sender, EventArgs e)
        {
            DumpTextbox.Text = "";
            if (TestOptionsRadioButtonList.SelectedIndex < 0)
            {
                ResultsTextbox.Text = "Please select a test to run!";
                return;
            }
            RunButton.Enabled = false;
            ResultsTextbox.Text = "Executing...";
            switch (TestOptionsRadioButtonList.SelectedIndex)
            {
                case 0: _run_test_single_store(true); break;
                case 1: _run_test_single_store(false); break;
                case 2: _run_test_multi_store(); break;
                case 3: _run_test_alert_sent_counter(); break;
                case 4: _run_test_alert_cap_stopper(); break;
                case 5: _run_test_deactivated_subscription(); break;
                case 6: _run_test_buffer_overrun(); break;
                case 7: _run_test_alert_reached(); break;
            }
            RunButton.Enabled = true;
        }

        private void _run_test_single_store(bool waitforended)
        {
            var test = new TestSingleStore(DumpMessage, DumpResults, (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)), StoreIdTextBox.Text, StoreDbTextBox.Text);
            test.Start(waitforended);
        }

        private void _run_test_multi_store()
        {
            var storeIdAry = StoreIdTextBox.Text.Split(new [] {" ", ","}, StringSplitOptions.None);
            var storeDbAry = StoreDbTextBox.Text.Split(new [] { " ", "," }, StringSplitOptions.None);
            var test = new TestMultiStore(
                DumpMessage, 
                DumpResults, 
                (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)),
                (int)System.Convert.ChangeType(NumStoresDropDownList.SelectedValue, typeof(int)),
                storeIdAry.ToList(),
                storeDbAry.ToList());
            test.Start();
        }

        private void _run_test_alert_sent_counter()
        {
            var test = new TestAlertSentCounter(DumpMessage, DumpResults);
            test.Start();
        }

        private void _run_test_buffer_overrun()
        {
            var test = new TestBufferOverrun(DumpMessage, DumpResults, (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)), StoreIdTextBox.Text, StoreDbTextBox.Text);
            test.Start();
        }

        private void _run_test_alert_cap_stopper()
        {
            var test = new TestAlertCapStopper(DumpMessage, DumpResults, (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)), StoreIdTextBox.Text, StoreDbTextBox.Text);
            test.Start();
        }

        private void _run_test_deactivated_subscription()
        {
            var test = new TestDeactivatedAccount(DumpMessage, DumpResults, (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)), StoreDbTextBox.Text);
            test.Start();
        }

        private void _run_test_alert_reached()
        {
            var test = new TestAlertReached(DumpMessage, DumpResults, (int)System.Convert.ChangeType(SelectNumTxReqDropdown.SelectedValue, typeof(int)), StoreDbTextBox.Text);
            test.Start();
        }

        private void _dump_message(string message)
        {
            DumpTextbox.Text = string.Format("{0} \r\n{1}", DumpTextbox.Text, message);
        }
 
        private void _dump_results(string message)
        {
            ResultsTextbox.Text = string.Format("{0} \r\n{1}", ResultsTextbox.Text,  message);
        }

        protected void TestOptionsRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TestOptionsRadioButtonList.SelectedIndex)
            {
                case 0:
                    {
                        StoreIdTextBox.Text = ASConfig.TestSingleStoreId;
                        StoreDbTextBox.Text = ASConfig.TestSingleStoreDb;
                        break;
                    }
                case 1:
                    {
                        StoreIdTextBox.Text = ASConfig.TestSingleStoreComplexId;
                        StoreDbTextBox.Text = ASConfig.TestSingleStoreComplexDb;
                        break;
                    }
                case 2:
                    {
                        StoreIdTextBox.Text = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                            ASConfig.TestMultiStoreId1,
                            ASConfig.TestMultiStoreId2,
                            ASConfig.TestMultiStoreId3,
                            ASConfig.TestMultiStoreId4,
                            ASConfig.TestMultiStoreId5,
                            ASConfig.TestMultiStoreId6,
                            ASConfig.TestMultiStoreId7,
                            ASConfig.TestMultiStoreId8,
                            ASConfig.TestMultiStoreId9,
                            ASConfig.TestMultiStoreId10);
                        StoreDbTextBox.Text = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                            ASConfig.TestMultiStoreDb1,
                            ASConfig.TestMultiStoreDb2,
                            ASConfig.TestMultiStoreDb3,
                            ASConfig.TestMultiStoreDb4,
                            ASConfig.TestMultiStoreDb5,
                            ASConfig.TestMultiStoreDb6,
                            ASConfig.TestMultiStoreDb7,
                            ASConfig.TestMultiStoreDb8,
                            ASConfig.TestMultiStoreDb9,
                            ASConfig.TestMultiStoreDb10);
                        break;
                    }
                case 3:
                    {
                        StoreIdTextBox.Text = "No need for Store ID";
                        StoreDbTextBox.Text = "No need for Store database";
                        break;
                    }
                case 4:
                    {
                        StoreIdTextBox.Text = ASConfig.TestAlertCapStopperId;
                        StoreDbTextBox.Text = ASConfig.TestAlertCapStopperDb;
                        break;
                    }
                case 5:
                    {
                        StoreIdTextBox.Text = "No need for Store ID";
                        StoreDbTextBox.Text = ASConfig.TestDeactivatedAccountDb; 
                        break;
                    }
                case 6:
                    {
                        StoreIdTextBox.Text = ASConfig.TestBufferOverrunId;
                        StoreDbTextBox.Text = ASConfig.TestBufferOverrunDb;
                        break;
                    }
                case 7:
                    {
                        StoreIdTextBox.Text = "No need for Store ID";
                        StoreDbTextBox.Text = ASConfig.TestAlertReachedDb;
                        break;
                    }
            }

            // multiple stores
            if (TestOptionsRadioButtonList.SelectedIndex == 2)
                NumStoresDropDownList.Enabled = true;
            else
                NumStoresDropDownList.Enabled = false;
            // buffer overrun
            if (TestOptionsRadioButtonList.SelectedIndex == 6)
            {

                SelectNumTxReqDropdown.Items[0].Selected = false;
                SelectNumTxReqDropdown.Items[11].Enabled = true;
                SelectNumTxReqDropdown.Items[11].Selected = true;
                SelectNumTxReqDropdown.Enabled = false;                
            }
            // unique viewer counter
            else if (TestOptionsRadioButtonList.SelectedIndex == 3)
            {
                SelectNumTxReqDropdown.Enabled = false;
            }
            else
            {
                SelectNumTxReqDropdown.Items[11].Selected = false;
                SelectNumTxReqDropdown.Items[11].Enabled = false;
                SelectNumTxReqDropdown.Items[0].Selected = true;
                SelectNumTxReqDropdown.Enabled = true;
            }
        }

        protected void ImportButton_Click(object sender, EventArgs e)
        {
            var test = new ImportTestData();
            if (SelectFileUpload.HasFile)
            {
                test.Import(SelectDbDropDownList.SelectedValue, SelectFileUpload.PostedFile, _dump_results);
            }
            else
                _dump_results("Please select a file to upload");
        }
    }
}