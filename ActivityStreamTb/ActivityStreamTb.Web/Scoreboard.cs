﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace ActivityStreamTb.Web
{
    public class Scoreboard
    {
        private RavenClient _raven;
        private int _warning;
        private int _fatal;
        private int _error;
        private int _success;
        private int _totalsuccess;
        private int _unpublished;
        private string _header;
        private string _trace;
        private LoadDumpDelegate _dumpMessage;

        public Scoreboard(RavenClient Raven, LoadDumpDelegate DumpMessage, int ExpUnpublished = 0)
        {
            _header = "";
            string logheader = "------------------------------------------------------";
            logheader = string.Format("{0} \r\n----------------- SCOREBOARD REPORT ------------------", logheader);
            logheader = string.Format("{0} \r\n------------------------------------------------------", logheader);
            logheader = string.Format("{0} \r\nExecution Date: {1} {2}:{3}:{4}", logheader, System.DateTime.Now.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")), System.DateTime.Now.Hour.ToString(), System.DateTime.Now.Minute.ToString(), System.DateTime.Now.Second.ToString());
            logheader = string.Format("{0} \r\n", logheader);
            _header += logheader;
            this._raven = Raven;
            this._warning = 0;
            this._fatal = 0;
            this._error = 0;
            this._success = 0;
            this._totalsuccess = 0;
            this._unpublished = ExpUnpublished;
            this._dumpMessage = DumpMessage;
            //_trace = _header;
            this._trace = "";
        }

        public void CheckTimeout(ListenerStopperDelegate stopper)
        {
            this._raven.CheckTimeout(stopper);
        }

        public TestCounter GetCounters()
        {
            return this._raven.GetDbCounters();
        }

        public void UpdateCounters(TestCounter s)
        {
            this._raven.UpdateCounters(s);
        }

        // placed in scoreboard for logging unexpected events
        public void Record(Order order, bool CheckComplete = false, ListenerStopperDelegate stopper = null)
        {
            // find item in db
            var success = this._raven.UpdateSentOrder(order);

            // unexpected event
            if (!success)
            {
                string msg = string.Format(" \r\n***WARNING: Failed to find entry in database! {0}", DateTime.UtcNow);
                msg = string.Format("{0} \r\n{1}", msg, order.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace = string.Format("{0} \r\n{1}", _trace, msg);
                this._warning += 1;
            }

            else
            {
                string msg = string.Format(" \r\n***SUCCESS: Data found and updated in database. {0}", DateTime.UtcNow);
                msg = string.Format("{0} \r\n{1}", msg, order.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace = string.Format("{0} \r\n{1}", _trace, msg);                 
                this._success += 1;
            }

            if (CheckComplete)
                this._raven.CheckCompleteness(stopper);
        }

        public string Report(TestStatus Status, string PerformanceDbName, int ExpUnpublished = 0, bool ExpectTimeout = false)
        {
            bool success = true;
            string msg = "";
            string result = _header;            

            if (Status == TestStatus.TIMEOUT && !ExpectTimeout)
            {
                success = false;
                result += " \r\n***FATAL: Test interrupted. Timeout reached! \r\n";
                this._fatal += 1;
            }

            // unsent test data
            List<Order> failed = this._raven.FetchUnsentOrders();
            if (failed.Count > 0)
            {
                success = false;
                msg = string.Format(" \r\n***ERROR: Failed to send data to AS API {0}", DateTime.UtcNow);
                foreach (Order f in failed)
                    msg = string.Format("{0} \r\n{1}", msg, f.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace += msg;
                this._error += failed.Count;
            }
            

            // unqueued orders
            List<Order> unqueued = this._raven.FetchUnqueuedOrders();
            if (unqueued.Count > 0)
            {
                success = false;
                msg = string.Format(" \r\n***ERROR: Failed to observe in Event Store {0}", DateTime.UtcNow);
                foreach (Order u in unqueued)
                    msg = string.Format("{0} \r\n{1}", msg, u.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace += msg;
                this._error += unqueued.Count;
            }
            

            // unbroadcasted orders
            List<Order> lost = this._raven.FetchLostOrders();
            if (lost.Count > 0)
            {
                success = false;
                msg = string.Format(" \r\n***ERROR: Failed to observe broadcasted data {0}", DateTime.UtcNow);
                foreach(Order o in lost)
                    msg = string.Format("{0} \r\n{1}", msg, o.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace += msg;
                this._error += lost.Count;
            }
            

            // unviewed orders
            List<Order> unviewed = this._raven.FetchUnViewedOrders();
            if (unviewed.Count > 0)
            {
                success = false;
                msg = string.Format(" \r\n***WARNING: The following were not viewed {0}", DateTime.UtcNow);
                foreach (Order v in unviewed)
                    msg = string.Format("{0} \r\n{1}", msg, v.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace += msg;
                this._error += unviewed.Count;
            }
            

            // unpublished orders
            List<Order> unpublished = this._raven.FetchNotPublishedOrders();
            if (unpublished.Count != ExpUnpublished)
            {
                success = false;
                _trace += string.Format(" \r\n***ERROR: Unpublished events :: Expected {0} != Observed {1} {2}", ExpUnpublished, unpublished.Count, DateTime.UtcNow);
                this._error += Math.Abs(unpublished.Count - ExpUnpublished);
            }

            if (unpublished.Count > 0)
            {
                msg = string.Format(" \r\n\r\nNOT PUBLISHED Events {0}", DateTime.UtcNow);
                foreach (Order p in unpublished)
                    msg = string.Format("{0} \r\n{1}", msg, p.Print());
                _trace += msg;
            }


            // unignored orders
            List<Order> unignored = this._raven.FetchUnIgnoredOrders();
            if (unignored.Count > 0)
            {
                success = false;
                msg = string.Format(" \r\n***ERROR: The following were not ignored {0}", DateTime.UtcNow);
                foreach (Order v in unignored)
                    msg = string.Format("{0} \r\n{1}", msg, v.Print());
                msg = string.Format("{0} \r\n", msg);
                _trace += msg;
                this._error += unignored.Count;
            }

            if (success)
                result += " \r\n***SUCCESS: All events requested were successful.\r\n";

            // successful
            var counter = new PerformanceCounter();
            List<Order> broadcasted = this._raven.FetchSuccessfulOrders();
            counter.totalCount = broadcasted.Count;
            foreach (Order order in broadcasted)
            {
                // calculate performance data
                order.CalculateMetrics();
                counter.AddSentQueuedTime(order.SentQueuedSpan);
                counter.AddQueuedBroadcastedTime(order.QueuedBroadcastedSpan);
                counter.AddBroadcastedViewedTime(order.BroadcastedViewedSpan);
                counter.AddTotalTime(order.TotalSpan);
                counter.AddViewCount(order.ViewCount);
            }
            this._totalsuccess += broadcasted.Count();

            // counters
            msg = "------------------------------------------------------";
            //msg = string.Format("{0} \r\nSUCCESS MESSAGES: {1}", msg, this._success.ToString());
            //msg = string.Format("{0} \r\nFATAL MESSAGES  : {1}", msg, this._fatal.ToString());
            //msg = string.Format("{0} \r\nERROR  MESSAGES : {1}", msg, this._error.ToString());
            //msg = string.Format("{0} \r\nWARNING MESSAGES: {1}", msg, this._warning.ToString());
            //msg = string.Format("{0} \r\n------------------------------------------------------", msg);

            msg = string.Format("{0} \r\n {1}", msg, this._raven.GetTestCounters().Print());
            /*
            msg = string.Format("{0} \r\nStore ID              : {1}", msg, this._raven.GetStoreId());
            msg = string.Format("{0} \r\nTotal orders prepared : {1} orders", msg, this._raven.GetTotalPreparedOrders().ToString());
            msg = string.Format("{0} \r\nSent orders           : {1} orders", msg, this._raven.GetTotalSentOrders().ToString());
            msg = string.Format("{0} \r\nQueued orders         : {1} orders", msg, this._raven.GetTotalQueuedOrders().ToString());
            msg = string.Format("{0} \r\nBroadcasted orders    : {1} orders", msg, this._raven.GetTotalBroadcastedOrders().ToString());
            msg = string.Format("{0} \r\nViewed orders         : {1} orders", msg, this._raven.GetTotalViewedOrders().ToString());
            msg = string.Format("{0} \r\nUnpublished orders    : {1} orders", msg, this._raven.GetTotalUnpublishedOrders().ToString());
            msg = string.Format("{0} \r\nIgnored orders        : {1} orders", msg, this._raven.GetTotalIgnoredOrders().ToString());
            msg = string.Format("{0} \r\nFailed orders         : {1} orders", msg, this._raven.GetTotalFailedOrders().ToString());
            msg = string.Format("\r\n{0}\r\n------------------------------------------------------ \r\n", msg);
            */
            result += msg;

            // performance
            _raven.SavePerformanceCounters(counter, PerformanceDbName);
            //System.Threading.Thread.Sleep(500);
            //result += counter.GetPerformanceReport();
            //result += this._raven.GetOverallPerformanceReport(PerformanceDbName);

            _dumpMessage(_trace);

            return result;
        }
    }
}
