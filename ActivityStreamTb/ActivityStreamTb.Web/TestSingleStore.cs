﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestSingleStore
    {
        private string _storeId = ASConfig.TestSingleStoreId;
        private string _dbName = ASConfig.TestSingleStoreDb;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        int TotalTxRequest = 2;

        public TestSingleStore(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTxReq, string StoreId, string DbName)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this.TotalTxRequest = NumTxReq;
            this._storeId = StoreId;
            this._dbName = DbName;
        }

        public TestSingleStore(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTxReq)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this.TotalTxRequest = NumTxReq;
        }

        public TestCounter Start(bool WaitForEnded) 
        {
            // get token to access database
            var raven = new RavenClient(_storeId, _dumpMessage, _dbName);
            try
            {
                var token = raven.GetAccessToken();
                if (token < 1)
                {
                    _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", _dbName));
                    return null;
                }

                else
                {
                    // instantiate components
                    var sb = new Scoreboard(raven, _dumpMessage);
                    var sg = new ScenarioGenerator(raven, _dumpMessage);
                    var listener = new Listener(sb, _storeId, _dumpMessage);

                    // initialize data            
                    raven.InitializeTestData(_storeId);

                    // if wait for ended, 1 request at a time
                    int NumCycle = TotalTxRequest;
                    int NumRequestPerCycle = 1;

                    if (!WaitForEnded)
                    {
                        NumCycle = 1;
                        NumRequestPerCycle = TotalTxRequest;
                    }

                    for (int i = 0; i < NumCycle; i++)
                    {
                        // create listener thread
                        Thread listenT = new Thread(listener.Start);

                        // create timeout thread
                        Thread timeoutT = new Thread(listener.IsAliveChecker);

                        // prepare test data
                        raven.Prepare(NumRequestPerCycle);

                        // pre-start listener to get timestamps
                        listener.PreParse();
                        System.Threading.Thread.Sleep(3000);

                        // start scenario generator
                        if (!sg.Start(NumRequestPerCycle))
                            break;

                        // wait for alerts to be broadcasted
                        System.Threading.Thread.Sleep(NumRequestPerCycle * ASConfig.AlertInterval);

                        // start listener thread
                        listenT.Start();

                        // wait for listener thread to stop
                        listenT.Join();

                        if (listener.Status == TestStatus.TIMEOUT)
                            break;
                    }
                    Thread.Sleep(1000);
                    // create reports
                    this._dumpResults(sb.Report(listener.Status, ASConfig.RavenDbPerformanceDbName));                    
                }
            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }
            raven.ReturnAccessToken();
            var c = raven.GetTestCounters();

            return c;
        }
    }
}
