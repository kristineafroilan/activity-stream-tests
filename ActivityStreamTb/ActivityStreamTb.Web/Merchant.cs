﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityStreamTb.Web
{
    public class Merchant
    {
        public string StoreId { get; set; }
        public string Email { get; set; }
        public int CurrentUsage { get; set; }
        public int AlertCap { get; set; }
        public bool IsLimitReached { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PlanName { get; set; }
        public int AlertsViewed { get; set; }
        public int NotPublished { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime LastCutOffDate { get; set; }

        public string Print()
        {
            if (Email != null)
            {
                if (Email.Length > 30)
                    return string.Format("Id: {0} :: Cap: {1} :: Usage: {2} :: Email: {3}...", StoreId, AlertCap.ToString().PadRight(7), CurrentUsage.ToString().PadRight(7), Email.Substring(0, 26));
                else
                    return string.Format("Id: {0} :: Cap: {1} :: Usage: {2} :: Email: {3}", StoreId, AlertCap.ToString().PadRight(7), CurrentUsage.ToString().PadRight(7), Email);
            }
            else
                return string.Format("Id: {0} :: Cap: {1} :: Usage: {2}", StoreId, AlertCap.ToString().PadRight(7), CurrentUsage.ToString().PadRight(7));
        }

        public static string PrintHeader()
        {
            return string.Format("{0} :: {1} :: {2} :: {3} :: {4} :: {5}", "Name".PadRight(24), "Plan".PadRight(12), "Limit".PadRight(7), "Sent".PadRight(7), "Viewed".PadRight(7), "NotPub".PadRight(7));
        }

        public string PrintAll()
        {
            return string.Format("{0} :: {1} :: {2} :: {3} :: {4} :: {5}", (FirstName + " " + LastName).PadRight(24), PlanName.PadRight(12), AlertCap.ToString().PadRight(7), CurrentUsage.ToString().PadRight(7), AlertsViewed.ToString().PadRight(7), NotPublished.ToString().PadRight(7));
        }

        public void GetCutOffDate()
        {
            this.LastCutOffDate = this.CreatedDate;
            DateTime RunningMonth = this.CreatedDate;

            while (true)
            {
                RunningMonth = RunningMonth.AddMonths(1);
                if (RunningMonth < DateTime.UtcNow)
                    LastCutOffDate = RunningMonth;
                else
                    break;
            }
        }
    }
}