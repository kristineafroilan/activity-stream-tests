﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestAlertCapStopper
    {
        private string _storeId = ASConfig.TestAlertCapStopperId;
        private string _dbName = ASConfig.TestAlertCapStopperDb;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        // global
        int _numtx;
        RavenClient _raven;

        public TestAlertCapStopper(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx, string StoreId, string DbName)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numtx = NumTx;
            this._storeId = StoreId;
            this._dbName = DbName;
        }

        public TestAlertCapStopper(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numtx = NumTx;
        }

        public TestCounter Start()
        {
            // get access token from database
            this._raven = new RavenClient(_storeId, _dumpMessage, _dbName);
            if (this._raven.GetAccessToken() < 1)
            {
                _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", _storeId));
                return null;
            }

            try
            {
                // send data requests to reach alert cap
                var store = AmazonSqlClient.FetchStoreAcct(_storeId);
                store.CurrentUsage = this._getUsage(store);

                // ensure that account is always reaching alert cap
                if (store.CurrentUsage >= store.AlertCap)
                {
                    if(store.CurrentUsage > store.AlertCap)
                        _dumpResults(string.Format("***ERROR: Usage > Alert Cap! \r\n{0}", store.Print()));
                    store.AlertCap = (int) Math.Ceiling(store.CurrentUsage * 1.12);
                    AmazonSqlClient.SetAlertCap(_storeId, store.AlertCap);
                    AmazonSqlClient.WaitLimitReached(false, store.StoreId);
                }

                var success = this._sendSuccess(store.AlertCap - 1, store.CurrentUsage);
                System.Threading.Thread.Sleep(60000);
                store.CurrentUsage = this._getUsage(store);
                success &= this._sendSuccess(store.AlertCap, store.CurrentUsage);
                System.Threading.Thread.Sleep(60000);

                if (success)
                {
                    // validate that alert usage has reached alert cap
                    store.CurrentUsage = this._getUsage(store);
                    //if (usage != cap)
                    if (store.CurrentUsage < store.AlertCap)
                    {
                        _dumpResults(string.Format("***ERROR: Was not able to reach alert cap. Usage {0} != Alert Cap {1}", store.CurrentUsage, store.AlertCap));
                        this._raven.ReturnAccessToken();
                        return this._raven.GetTestCounters();
                    }
                    
                    AmazonSqlClient.WaitLimitReached(true, store.StoreId);
                    // unpublished
                    _sendUnpublished();
                }
            }

            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }

            this._raven.ReturnAccessToken();
            return this._raven.GetTestCounters();
        }

        private bool _sendSuccess(int cap, int usage)
        {
            var success = true;
            // instantiate components
            var sb = new Scoreboard(this._raven, _dumpMessage);
            var sg = new ScenarioGenerator(this._raven, _dumpMessage);
            var listener = new Listener(sb, _storeId, _dumpMessage);

            // this._create(raven, ClusterSize);
            int size = ASConfig.TestAlertCapClusterSize;
            int cycle = ((cap - usage) / size) + ((cap - usage) % size > 0 ? 1 : 0);
            int reqpercycle = 1;

            for (int i = 0; i < cycle; i++)
            {
                // reset test data
                this._raven.InitializeTestData(_storeId);
                //size = (i < cycle - 1) ? ASConfig.TestAlertCapClusterSize : ((cap - usage) % ASConfig.TestAlertCapClusterSize == 0 ? ASConfig.TestAlertCapClusterSize : (cap - usage) % ASConfig.TestAlertCapClusterSize);
                size = ((i == cycle - 1) && (cap - usage) % ASConfig.TestAlertCapClusterSize > 0) ? (cap - usage) % ASConfig.TestAlertCapClusterSize : ASConfig.TestAlertCapClusterSize;
                this._raven.PrepareClusteredDataRequest(_storeId, this._raven.GetCurrentTxId(), size);

                // create threads
                Thread listenT = new Thread(listener.Start);
                Thread timeoutT = new Thread(listener.IsAliveChecker);

                // pre-start listener to get timestamps
                listener.PreParse();

                _dumpResults(string.Format("Sending successful transactions: {0}", reqpercycle));
                // start
                if (!sg.Start(reqpercycle))
                {
                    success = false;
                    break;
                }

                System.Threading.Thread.Sleep(reqpercycle * ASConfig.AlertInterval);

                listenT.Start();

                // wait for listener thread to stop
                listenT.Join();

                if (listener.Status == TestStatus.TIMEOUT)
                {
                    success = false;
                    break;
                }
            }
            return success;
        }

        private void _sendUnpublished()
        {
            // instantiate components
            var sb = new Scoreboard(this._raven, _dumpMessage);
            var sg = new ScenarioGenerator(this._raven, _dumpMessage);
            var listener = new Listener(sb, _storeId, _dumpMessage);

            // reset test data
            this._raven.InitializeTestData(_storeId);
            this._raven.PrepareClusteredDataRequest(_storeId, this._raven.GetCurrentTxId(), this._numtx);
            
            // create threads
            Thread listenT = new Thread(listener.Start);
            Thread timeoutT = new Thread(listener.IsAliveChecker);

            // pre-start listener to get timestamps
            listener.PreParse();
            System.Threading.Thread.Sleep(3000);

            // start
            sg.Start(1);
            listenT.Start();

            // wait for listener thread to stop
            listenT.Join();

            Thread.Sleep(1000);
            // create reports
            this._dumpResults(sb.Report(listener.Status, ASConfig.RavenDbPerformanceDbName, this._numtx));
        }

        private void _create(RavenClient raven, int num)
        {
            for (int i = 0; i < num; i++)
            {
                var o = new Order()
                {
                    TxId = i.ToString(),
                    StoreId = _storeId,
                    FirstName = string.Format("FirstName:{0}", i),
                    Item = string.Format("Alert Cap Item:{0}", i),
                    Quantity = i,
                    Price = i.ToString()
                };
                raven.CreateOrder(o);
            }
        }

        private int _getUsage(Merchant store)
        {
            var listener = new Listener(_dumpMessage);
            try
            {
                return listener.CountAlertSent(store);
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: Failed to get existing usage count.\r\n{0}", e.Message));
            }
            return 0;
        }
    }
}
