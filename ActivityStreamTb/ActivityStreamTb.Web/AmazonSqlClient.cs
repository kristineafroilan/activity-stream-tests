﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace ActivityStreamTb.Web
{
    public static class AmazonSqlClient
    {
        public static SqlConnection GetSqlConnection()
        {
            SqlConnection conn = new SqlConnection(ASConfig.AmazonConnectionString);
            return conn;
        }

        public static SqlDataReader GetQuery(string query, SqlConnection conn)
        {
            SqlDataReader reader = null;
            SqlCommand cmd = new SqlCommand(query, conn);
            reader = cmd.ExecuteReader();
            return reader;
        }

        public static Merchant FetchStoreAcct(string storeid)
        {
            var store = new Merchant();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            //SqlDataReader rdr = GetQuery("select * from dbo.Stores", conn);
            /*
            SqlDataReader rdr = GetQuery(string.Format(
                "select dbo.Stores.StoreId, dbo.Stores.TotalNotificationAllowed " +
                "from dbo.Stores where dbo.Stores.StoreId = '{0}'", storeid), conn);
            
            while (rdr.Read())
            {
                // get items here
                store.StoreId = rdr["StoreId"].ToString();
                store.AlertCap = (int)System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
            }
            */

            SqlDataReader rdr = GetQuery(string.Format(
                "select * " +
                "FROM dbo.Stores " +
                "INNER JOIN dbo.Users " +
                "ON dbo.Users.Username = dbo.Stores.StoreId where dbo.Stores.StoreId = '{0}'", storeid), conn);

            while (rdr.Read())
            {
                // get items here
                store.StoreId = rdr["StoreId"].ToString();
                store.Email = rdr["Email"].ToString();
                store.AlertCap = (int)System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
                store.IsLimitReached = (bool)System.Convert.ChangeType(rdr["LimitReached"], typeof(bool));
                store.CreatedDate = DateTime.Parse(rdr["CreatedDate"].ToString());
            }

            rdr.Close();
            conn.Close();

            store.GetCutOffDate();
            return store;
        }

        public static List<Merchant> FetchActiveStores()
        {
            var list = new List<Merchant>();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            //SqlDataReader rdr = GetQuery("select * from dbo.Stores", conn);
            SqlDataReader rdr = GetQuery(
                //"select dbo.Stores.StoreId, dbo.Stores.TotalNotificationAllowed, dbo.Users.Email, dbo.Stores.LimitReached " +
                "select * " +
                "FROM dbo.Stores " +
                "INNER JOIN dbo.Users " +
                "ON dbo.Users.Username = dbo.Stores.StoreId " +
                "where dbo.Users.IsDeleted = 0", conn);
            while (rdr.Read())
            {
                // get items here
                var store = new Merchant();
                store.StoreId = rdr["StoreId"].ToString();
                store.Email = rdr["Email"].ToString();
                store.AlertCap = (int) System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
                store.IsLimitReached = (bool)System.Convert.ChangeType(rdr["LimitReached"], typeof(bool));
                store.CreatedDate = DateTime.Parse(rdr["CreatedDate"].ToString());
                store.GetCutOffDate();
                list.Add(store);
            }

            rdr.Close();
            conn.Close();

            return list;
        }

        public static List<Merchant> FetchSubscriptions()
        {
            var list = new List<Merchant>();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            //SqlDataReader rdr = GetQuery("select * from dbo.Stores", conn);
            SqlDataReader rdr = GetQuery(
                "select * " +
                "FROM dbo.Users " +
                "INNER JOIN dbo.Stores on dbo.Stores.StoreId = dbo.Users.Username " +
                "inner join dbo.Subscriptions on dbo.Stores.id = dbo.subscriptions.storeid " +
                "inner join dbo.subscriptionpackages on dbo.subscriptions.subscriptionpackageid = dbo.subscriptionpackages.id " +
                "where dbo.users.roleid = 1 and dbo.users.isactivated = 1 and dbo.users.isdeleted = 0", conn);
            while (rdr.Read())
            {
                // get items here
                var store = new Merchant();
                store.StoreId = rdr["StoreId"].ToString();
                store.Email = rdr["Email"].ToString();
                store.AlertCap = (int)System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
                store.IsLimitReached = (bool)System.Convert.ChangeType(rdr["LimitReached"], typeof(bool));
                store.FirstName = rdr["FirstName"].ToString();
                store.LastName = rdr["LastName"].ToString();
                store.PlanName = rdr["Name"].ToString();
                store.GetCutOffDate();
                list.Add(store);
            }

            rdr.Close();
            conn.Close();

            return list;
        }

        public static List<Merchant> FetchDeactivatedStores()
        {
            var list = new List<Merchant>();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery(
                "select dbo.Stores.StoreId, dbo.Stores.TotalNotificationAllowed, dbo.Users.Email, dbo.Stores.LimitReached " +
                "FROM dbo.Stores " +
                "INNER JOIN dbo.Users "+
                "ON dbo.Users.Username = dbo.Stores.StoreId and dbo.Users.IsDeleted = 1", conn);
            while (rdr.Read())
            {
                // get items here
                var store = new Merchant();
                store.StoreId = rdr["StoreId"].ToString();
                store.Email = rdr["Email"].ToString();
                store.AlertCap = (int)System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
                store.IsLimitReached = (bool)System.Convert.ChangeType(rdr["LimitReached"], typeof(bool));
                store.GetCutOffDate();
                list.Add(store);
            }

            rdr.Close();
            conn.Close();

            return list;
        }

        public static StringCollection FetchTestUsers()
        {
            var list = new StringCollection();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery("select * from dbo.Users where FirstName like 'Test%'", conn);
            while (rdr.Read())
            {
                // get email items here
                list.Add(rdr["Email"].ToString());
            }

            rdr.Close();
            conn.Close();

            return list;
        }

        public static int GetAlertCap(string storeid)
        {
            int cap = 0;
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery(string.Format("select * from dbo.Stores where StoreId='{0}'", storeid), conn);
            while (rdr.Read())
            {
                cap = (int)System.Convert.ChangeType(rdr["TotalNotificationAllowed"], typeof(int));
                break;
            }

            rdr.Close();
            conn.Close();

            return cap;
        }

        public static void SetAlertCap(string storeid, int cap)
        {
            SqlConnection conn = GetSqlConnection();
            conn.Open();
            SqlDataReader rdr = GetQuery(string.Format(
                "update dbo.Stores " +
                "set TotalNotificationAllowed={0}, " +
                "dbo.Stores.QuotaReachedEmailSent=0, " +
                "dbo.Stores.QuotaWarningEmailSent=0, " +
                "dbo.Stores.LimitReached=0" +
                "where dbo.Stores.StoreId = '{1}'", cap, storeid), conn);
            rdr.Close();
            conn.Close();
        }

        public static List<Merchant> GetStoreId()
        {
            var list = new List<Merchant>();
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            //SqlDataReader rdr = GetQuery("select * from dbo.Stores", conn);
            SqlDataReader rdr = GetQuery("select * from dbo.Users where Email like 'kristine+tbms%'", conn);
            while (rdr.Read())
            {
                // get items here
                var store = new Merchant();
                store.StoreId = rdr["Username"].ToString();
                store.Email = rdr["Email"].ToString();
                store.GetCutOffDate();
                if(store.StoreId != "412af925b7914ba8b016f28c6c1d8578")
                    list.Add(store);
            }

            rdr.Close();
            conn.Close();

            return list;
        }

        public static void WaitLimitReached(bool IsReached, string storeId)
        {
            SqlConnection conn = GetSqlConnection();

            conn.Open();

            while (true)
            {
                var limitreached = !IsReached;
                SqlDataReader rdr = GetQuery(string.Format("select * from dbo.Stores where StoreId = '{0}'", storeId), conn);
                while (rdr.Read())
                {
                    limitreached = (bool)System.Convert.ChangeType(rdr["LimitReached"], typeof(bool));
                }
                rdr.Close();
                if (limitreached == IsReached)
                    break;
                else
                    System.Threading.Thread.Sleep(1000);
            }
            conn.Close();
        }
    }
}