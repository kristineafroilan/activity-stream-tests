﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityStreamTb.Web
{
    public class TestCounter
    {
        public int TotalPrepared { get; set; }
        public int TotalSent { get; set; }
        public int TotalQueued { get; set; }
        public int TotalBroadcasted { get; set; }
        public int TotalViewed { get; set; }
        public int TotalUnpublished { get; set; }
        public int TotalIgnored { get; set; }
        public int TotalFailed { get; set; }

        public DateTime LastParseQueued { get; set; }
        public DateTime LastParseBroadcasted { get; set; }
        public DateTime LastParseViewed { get; set; }
        public DateTime LastParseUnpublished { get; set; }
        public bool IsIdle { get; set; }

        public string StoreId { get; set; }

        public TestCounter()
        {
            TotalPrepared = 0;
            TotalSent = 0;
            TotalQueued = 0;
            TotalBroadcasted = 0;
            TotalViewed = 0;
            TotalUnpublished = 0;
            TotalIgnored = 0;
            TotalFailed = 0;
        }

        public void Add(TestCounter obj)
        {
            if (obj != null)
            {
                this.TotalPrepared += obj.TotalPrepared;
                this.TotalSent += obj.TotalSent;
                this.TotalQueued += obj.TotalQueued;
                this.TotalBroadcasted += obj.TotalBroadcasted;
                this.TotalViewed += obj.TotalViewed;
                this.TotalUnpublished += obj.TotalUnpublished;
                this.TotalIgnored += obj.TotalIgnored;
                this.TotalFailed += obj.TotalFailed;
            }
        }

        public TestCounter CopyTo()
        {
            var obj = new TestCounter();
            obj.TotalPrepared = this.TotalPrepared;
            obj.TotalSent = this.TotalSent;
            obj.TotalBroadcasted = this.TotalBroadcasted;
            obj.TotalViewed = this.TotalViewed;
            obj.TotalQueued = this.TotalQueued;
            obj.TotalUnpublished = this.TotalUnpublished;
            obj.TotalIgnored = this.TotalIgnored;
            obj.TotalFailed = this.TotalFailed;
            obj.LastParseQueued = this.LastParseQueued;
            obj.LastParseBroadcasted = this.LastParseBroadcasted;
            obj.LastParseViewed = this.LastParseViewed;
            obj.LastParseUnpublished = this.LastParseUnpublished;
            obj.IsIdle = this.IsIdle;
            obj.StoreId = this.StoreId;

            return obj;
        }

        public string Print()
        {
            string msg = "";
            if (this.StoreId != null)
                msg = string.Format("{0} \r\n Store ID              : {1}", msg, this.StoreId);
            msg = string.Format("{0} \r\n Total orders prepared : {1} orders", msg, this.TotalPrepared.ToString());
            msg = string.Format("{0} \r\n Sent orders           : {1} orders", msg, this.TotalSent.ToString());
            msg = string.Format("{0} \r\n Queued orders         : {1} orders", msg, this.TotalQueued.ToString());
            msg = string.Format("{0} \r\n Broadcasted orders    : {1} orders", msg, this.TotalBroadcasted.ToString());
            msg = string.Format("{0} \r\n Viewed orders         : {1} orders", msg, this.TotalViewed.ToString());
            msg = string.Format("{0} \r\n Unpublished orders    : {1} orders", msg, this.TotalUnpublished.ToString());
            msg = string.Format("{0} \r\n Ignored orders        : {1} orders", msg, this.TotalIgnored.ToString());
            msg = string.Format("{0} \r\n Failed orders         : {1} orders\r\n", msg, this.TotalFailed.ToString());
            return msg;
        }
    }
}