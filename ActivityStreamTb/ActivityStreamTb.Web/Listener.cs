﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public enum TestStatus
    {
        NONE,
        SUCCESS,
        FAILED,
        TIMEOUT
    }

    public delegate void ListenerStopperDelegate(TestStatus Status);

    public class Listener
    {
        private Scoreboard _sb;
        private string _storeId;
        private volatile bool _isAlive = true;
        public ListenerStopperDelegate Stopper;
        public TestStatus Status;

        private TestCounter _counter;
        private LoadDumpDelegate _dumpMessage;

        public Listener(LoadDumpDelegate DumpMessage)
        {
            this._dumpMessage = DumpMessage;
        }

        public Listener(Scoreboard sb, string StoreId, LoadDumpDelegate DumpMessage) 
        {
            this._sb = sb;
            this._storeId = StoreId;
            this._isAlive = true;
            Stopper = new ListenerStopperDelegate(RequestStop);
            Status = TestStatus.NONE;
            this._counter = new TestCounter();
            this._dumpMessage = DumpMessage;
        }

        public void RequestStop(TestStatus Status)
        {
            this._isAlive = false;
            this.Status = Status;
        }

        public void PreParse()
        {
            _dumpMessage("***DEBUG: Listener.PreParse()::Parsing existing events in event store...");
            this._counter = this._sb.GetCounters();
            // parse all existing events prior to run to update counters
            ParseOrderQueued();
            ParseNotificationSent();
            ParseNotificationReceived();
            ParseNotPublished();
        }

        public void Start()
        {
            _dumpMessage("***DEBUG: Listener.Start()::Listener started.");
            this._isAlive = true;

            // spawn thread for checking timeout
            Thread t = new Thread(IsAliveChecker);
            t.Start();

            while (this._isAlive)
            {
                // poll every x seconds
                System.Threading.Thread.Sleep(3000);

                // parse OrderQeueued
                var orderAry = ParseOrderQueued();

                // parse NotificationSent
                var notifSentAry = ParseNotificationSent();

                // parse NotificationReceived
                var notifReceivedAry = ParseNotificationReceived();

                // parse NotPublished
                var notPublishedAry = ParseNotPublished();

                // update scoreboarding in database
                foreach (Order oq in orderAry)
                    this._sb.Record(oq);

                foreach (Order ns in notifSentAry)
                    this._sb.Record(ns);

                for (int i = 0; i < notifReceivedAry.Count; i++)
                {
                    if (i == (notifReceivedAry.Count - 1))
                        this._sb.Record(notifReceivedAry[i], true, Stopper);
                    else
                        this._sb.Record(notifReceivedAry[i]);
                }

                for (int i = 0; i < notPublishedAry.Count; i++)
                {
                    if (i == (notPublishedAry.Count - 1))
                        this._sb.Record(notPublishedAry[i], true, Stopper);
                    else
                        this._sb.Record(notPublishedAry[i]);
                }
            }
            // kill timeout thread
            t.Abort();
        }

        public void IsAliveChecker()
        {
            _dumpMessage("***DEBUG: Listener.IsAliveChecker()::Alive checker started.");
            while (this._isAlive)
            {
                // sleep every 30 seconds
                System.Threading.Thread.Sleep(30000);
                this._sb.CheckTimeout(Stopper);
            }
        }

        private List<Order> ParseOrderQueued()
        {
            var orders = new List<Order>();

            DateTime latestDateTime = new DateTime();

            // parse OrderQeueued
            var uri = ASConfig.EventStoreTbOrderQueuedUrl + _storeId ;
            var json = DownloadJson(uri);
            var list = JsonConvert.DeserializeObject<OrderQueuedEventItems>(json);
            if(list != null)
            {
                list.Items.Sort((x, y) => DateTime.Compare(x.Date, y.Date));

                foreach (OrderQueuedEvent ei in list.Items)
                {
                    // parse only new events
                    if (ei.Date > this._counter.LastParseQueued)
                    {
                        orders.Add(ei.ToOrder(this._storeId));
                        //this._sb.SaveOrderQueuedEvent(ei);
                        //this._counter.TotalQueued += 1;
                        if (ei.Date > latestDateTime)
                            latestDateTime = ei.Date;
                    }
                }
            }
            if(latestDateTime > this._counter.LastParseQueued)
                this._counter.LastParseQueued = latestDateTime;
            this._sb.UpdateCounters(this._counter);

            return orders;
        }

        private List<Order> ParseNotificationSent()
        {
            var orders = new List<Order>();
            DateTime latestDateTime = new DateTime();

            // parse OrderQeueued
            var uri = ASConfig.EventStoreTbNotificationSentUrl + _storeId;
            var json = DownloadJson(uri);
            var list = JsonConvert.DeserializeObject<NotificationSentEventItems>(json);
            if(list != null)
            {
                list.Items.Sort((x, y) => DateTime.Compare(x.Date, y.Date));

                foreach (NotificationSentEvent ei in list.Items)
                {
                    // parse only new events
                    if (ei.Date > this._counter.LastParseBroadcasted)
                    {
                        orders.Add(ei.ToOrder(this._storeId));
                        //this._sb.SaveNotificationSentEvent(ei);
                        //this._counter.TotalBroadcasted += 1;
                        if (ei.Date > latestDateTime)
                            latestDateTime = ei.Date;
                    }
                }
            }

            if (latestDateTime > this._counter.LastParseBroadcasted)
                this._counter.LastParseBroadcasted = latestDateTime;
            this._sb.UpdateCounters(this._counter);

            return orders;
        }

        private List<Order> ParseNotificationReceived()
        {
            var orders = new List<Order>();
            DateTime latestDateTime = new DateTime();
            NotificationReceivedEvent lastWidget = null;

            // parse OrderQeueued
            var uri = ASConfig.EventStoreTbNotificationReceivedUrl + _storeId;
            var json = DownloadJson(uri);
            var list = JsonConvert.DeserializeObject<NotificationReceivedEventItems>(json);
            if(list != null)
            {
                list.Items.Sort((x, y) => DateTime.Compare(x.Date, y.Date));

                foreach (NotificationReceivedEvent ei in list.Items)
                {
                    // parse only new events
                    if (ei.Date > this._counter.LastParseViewed)
                    {
                        orders.Add(ei.ToOrder(this._storeId));
                        // only save unique widget messages
                        if (lastWidget == null || !ei.Message.Equals(lastWidget.Message))
                        {
                            //this._sb.SaveNotificationReceivedEvent(ei);
                            //this._counter.TotalViewed += 1;
                            if (ei.Date > latestDateTime)
                                latestDateTime = ei.Date;
                            lastWidget = ei.CopyTo();
                        }
                    }
                }
            }

            if(latestDateTime > this._counter.LastParseViewed)
                this._counter.LastParseViewed = latestDateTime;
            this._sb.UpdateCounters(this._counter);

            return orders;
        }

        private List<Order> ParseNotPublished()
        {
            var orders = new List<Order>();

            DateTime latestDateTime = new DateTime();

            // parse NotPublished
            var uri = ASConfig.EventStoreTbNotPublishedUrl + _storeId;
            var json = DownloadJson(uri);
            var list = JsonConvert.DeserializeObject<NotPublishedEventItems>(json);
            if (list != null)
            {
                list.Items.Sort((x, y) => DateTime.Compare(x.Date, y.Date));

                foreach (NotPublishedEvent ei in list.Items)
                {
                    // parse only new events
                    if (ei.Date > this._counter.LastParseUnpublished)
                    {
                        orders.Add(ei.ToOrder(this._storeId));
                        //this._sb.SaveNotPublishedEvent(ei);
                        //this._counter.TotalUnpublished += 1;
                        if (ei.Date > latestDateTime)
                            latestDateTime = ei.Date;
                    }
                }
            }
            if (latestDateTime > this._counter.LastParseUnpublished)
                this._counter.LastParseUnpublished = latestDateTime;
            this._sb.UpdateCounters(this._counter);

            return orders;
        }

        public int CountUniqueWidget(string storeid)
        {
            try
            {
                var uri = ASConfig.EventStoreTbNotificationReceivedUrl + storeid;
                var json = DownloadJson(uri, storeid);
                var list = JsonConvert.DeserializeObject<NotificationReceivedEventItems>(json);
                if (list != null)
                {
                    var messages = list.Items
                        .GroupBy(msg => msg.Message + " " + msg.UserCookie)
                        .Select(g => new
                        {
                            Message = g.Key,
                            //Count = g.Select(l => l.Date).Distinct().Count()
                            Count = g.Select(l => l.Message + l.UserCookie).Distinct().Count()
                        });
                    int counter = 0;
                    foreach (var g in messages)
                    {
                        //_dumpMessage(string.Format("{0} :: {1} :: {2}", storeid, g.Message, g.Count));
                        counter += g.Count;
                    }
                    return counter;
                }
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: Listener.CountUniqueWidget()::Exception encountered for Store {0}\r\n{1}", storeid, e.Message));
            }

            return 0;
        }

        public int CountAlertSent(Merchant store)
        {
            int count = 0;

            try
            {
                var uri = ASConfig.EventStoreTbNotificationSentUrl + store.StoreId;
                var json = DownloadJson(uri, store.StoreId);
                var list = JsonConvert.DeserializeObject<NotificationSentEventItems>(json);
                

                if (list != null)
                {
                    foreach (NotificationSentEvent n in list.Items)
                    {
                        if (n.Date >= store.LastCutOffDate)
                            count += 1;
                    }
                    //return list.Items.Count;
                }
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: Listener.CountAlertSent()::Exception encountered for Store {0}\r\n{1}", store.StoreId, e.Message));
            }

            return count;
        }        

        public int CountNotPublished(string storeid)
        {
            try
            {
                var uri = ASConfig.EventStoreTbNotPublishedUrl + storeid;
                var json = DownloadJson(uri, storeid);
                var list = JsonConvert.DeserializeObject<NotPublishedEventItems>(json);

                if (list != null)
                {
                    return list.Items.Count;
                }
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: Listener.CountNotPublished()::Exception encountered for Store {0}\r\n{1}", storeid, e.Message));
            }

            return 0;
        }

        private string DownloadJson(string uri)
        {
            return DownloadJson(uri, this._storeId);
        }

        private string DownloadJson(string uri, string storeid)
        {
            WebClient webclient = new WebClient();
            webclient.Credentials = new NetworkCredential(ASConfig.EventStoreUname, ASConfig.EventStorePword);
            webclient.Headers.Add(ASConfig.ASApiHeaderType, ASConfig.ASApiHeaderValue);

            string json;
            while (true)
            {
                try
                {
                    json = webclient.DownloadString(uri);
                    break;
                }
                catch (Exception e)
                {
                    _dumpMessage(string.Format("***ERROR: Unable to download data from \r\n{0}!\r\n{1}", uri, e.Message));
                }
            }

            if(!json.Equals(""))
                json = json.Replace(storeid, "Items");
            return json;
        }
    }
}
