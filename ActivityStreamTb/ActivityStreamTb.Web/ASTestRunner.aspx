﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ASTestRunner.aspx.cs" Inherits="ActivityStreamTb.Web.ASTestRunner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1 {
            font-family: cursive;
        }
        .newStyle2 {
            font-family: "Trebuchet MS", "Lucida Sans Unicode", "Lucida Grande", "Lucida Sans", Arial, sans-serif;
        }
    </style>
</head>

<script id="myscript" type="text/javascript">
    var _activityStream = _activityStream || [];
    (function () {
        var myscript = document.createElement('script');
        myscript.type = 'text/javascript';
        myscript.src = '//d12vv1dxyda1qs.cloudfront.net/stores/950e193d3bb741a99ab360e9b8ca71f1/activitystream.js';
        var s = document.getElementById('myscript');
        s.parentNode.insertBefore(myscript, s);
    })();
    _activityStream.push(['create', '950e193d3bb741a99ab360e9b8ca71f1']);
</script>

<body>
    <form id="form1" runat="server">
        <div style="margin-left: 40px; height: 60px">
        </div>
        <div style="margin-left:40px; height: 40px; width: 250px; font-family:'Segoe UI'; font-size:large; font-weight:bold">
            SELECT A TEST TO RUN</div>
        <p style="margin: 2px 2px 2px 40px; font-family:'Segoe UI'; font-size:small">
            <asp:RadioButtonList ID="TestOptionsRadioButtonList" runat="server" OnSelectedIndexChanged="TestOptionsRadioButtonList_SelectedIndexChanged" style="margin: 2px 2px 2px 40px; font-family:'Segoe UI'; font-size:small" AutoPostBack="True">
                <asp:ListItem>Simple Data Request</asp:ListItem>
                <asp:ListItem>Complex Data Request</asp:ListItem>
                <asp:ListItem>Multiple Store Data Request</asp:ListItem>
                <asp:ListItem>Alert Sent Counter</asp:ListItem>
                <asp:ListItem>Alert Cap Stopper</asp:ListItem>
                <asp:ListItem>Deactivated Subscription</asp:ListItem>
                <asp:ListItem>Queuer Data Overrun</asp:ListItem>
                <asp:ListItem>Alert Reached</asp:ListItem>
            </asp:RadioButtonList>
        </p>

        <div style="margin-left:40px; font-family:'Segoe UI'; font-size:small; height: 29px">
            Select number of order requests to send :   &nbsp; <asp:DropDownList ID="SelectNumTxReqDropdown" runat="server" Font-Names="Segoe UI" Font-Size="Small" Height="25px">
                <asp:ListItem Selected="True">1</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>25</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>35</asp:ListItem>
                <asp:ListItem>40</asp:ListItem>
                <asp:ListItem>45</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem Enabled="False">150</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div style="margin-left:40px; font-family:'Segoe UI'; font-size:small; height: 29px">
            Select number of test stores to instantiate:   &nbsp;
            <asp:DropDownList ID="NumStoresDropDownList" runat="server" Font-Names="Segoe UI" Font-Size="Small" Height="25px" Enabled="False">
                <asp:ListItem Selected="True">2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem Enabled="False">5</asp:ListItem>
                <asp:ListItem Enabled="False">6</asp:ListItem>
                <asp:ListItem Enabled="False">7</asp:ListItem>
                <asp:ListItem Enabled="False">8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div style="margin-left: 40px; font-family: 'segoe UI'; font-size: small;">
            Please enter Store ID&nbsp; :&nbsp; <asp:TextBox ID="StoreIdTextBox" runat="server" Width="280px" Font-Names="Segoe UI" Font-Size="Small" TextMode="MultiLine"></asp:TextBox>
        </div>

        <div style="margin-left: 40px; font-family: 'segoe UI'; font-size: small;">
            Please enter Database:&nbsp; <asp:TextBox ID="StoreDbTextBox" runat="server" Width="280px" Font-Names="Segoe UI" Font-Size="Small" TextMode="MultiLine"></asp:TextBox>
        </div>

        <div style="margin-left: 40px; height: 25px">
        </div>

        <div style="margin-left: 40px">
            <asp:Button ID="RunButton" runat="server" Text="RUN" OnClick="RunButton_Click" Font-Bold="True" Font-Names="Segoe UI" Font-Size="Large" Height="50px" Width="110px" />
        </div>            

        <div style="margin-left: 40px; height: 25px">
        </div>

        <div style="margin-left: 40px; height: 30px; font-family: 'Segoe UI'; font-size: medium; font-weight: bold; width: 366px;">
            IMPORT TEST DATA</div>
        <div style="margin-left: 40px; height: auto; font-family: 'segoe UI'; font-size: x-small; width: 850px;">
            INSTRUCTIONS:<br />
            1. Remove unnecessary columns and rows from Excel sheet (Google drive)<br />
            2. Download sheet as plain text <br />
            3. Open in Notepad++
            <br />
            4. Replace &quot;OrderItems&quot;: [ { with null 
            <br />
            5. Replace &quot;OrderItems&quot;:[ { with null 
            <br />
            6. Replace } ] with null 
            <br />
            7. Save.</div>
        <div style="margin-left: 40px; height: 15px">
        </div>

        <div style="margin-left: 40px; height: 30px; font-family: 'Segoe UI'; font-size: small; width: 366px;" font-family:'Segoe UI'>
            Select Database Name:
            <asp:DropDownList ID="SelectDbDropDownList" runat="server" style="margin-top: 0px" Font-Names="Segoe UI">
                <asp:ListItem>TestSingleStore</asp:ListItem>
                <asp:ListItem>TestMultiStore1</asp:ListItem>
                <asp:ListItem>TestMultiStore2</asp:ListItem>
                <asp:ListItem>TestMultiStore3</asp:ListItem>
                <asp:ListItem>TestMultiStore4</asp:ListItem>
                <asp:ListItem>TestMultiStore5</asp:ListItem>
                <asp:ListItem>TestMultiStore6</asp:ListItem>
                <asp:ListItem>TestMultiStore7</asp:ListItem>
                <asp:ListItem>TestMultiStore8</asp:ListItem>
                <asp:ListItem>TestMultiStore9</asp:ListItem>
                <asp:ListItem>TestMultiStore10</asp:ListItem>
                <asp:ListItem>TestAlertCapStopper</asp:ListItem>
                <asp:ListItem>TestDeactivatedAccount</asp:ListItem>
                <asp:ListItem>TestBufferOverrun</asp:ListItem>
                <asp:ListItem>Dummy</asp:ListItem>
                <asp:ListItem>TestAlertReached</asp:ListItem>
                <asp:ListItem>TestSingleStoreComplex</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div style="margin-left: 40px; height: 30px; font-family: 'segoe UI'; font-size: small; width: 366px;" dir="ltr">
            Select File: <asp:FileUpload ID="SelectFileUpload" runat="server" Font-Names="Segoe UI" />
        </div>

        <div style="margin-left: 40px; height: 15px">
        </div>

        <div style="margin-left: 40px; height: 40px; width: 366px;">
            <asp:Button ID="ImportButton" runat="server" Font-Names="Segoe UI" Font-Size="Small" OnClick="ImportButton_Click" Text="IMPORT" Height="40px" Width="100px" />
        &nbsp;
        </div>
        
        <br />
        <div style="margin-left:40px; height: 25px; font-family:'Segoe UI'; font-size:small;">
            Status: </div>
        <div style="margin-left: 40px" aria-expanded="true" aria-multiline="True">
            <asp:TextBox ID="ResultsTextbox" runat="server" BorderStyle="Dotted" Font-Names="Consolas" Font-Size="Small" Height="500px" style="margin-top: 0px" TextMode="MultiLine" Width="800px" AutoPostBack="True"></asp:TextBox>
        </div>
        <br />
        <br />

        <div style="margin-left:40px; height: 25px; font-family:'Segoe UI'; font-size:small;">
            Trace : </div>
        <div style="margin-left: 40px">
            <asp:TextBox ID="DumpTextbox" runat="server" BorderStyle="Dotted" Font-Names="Consolas" Font-Size="Small" Height="500px" style="margin-top: 0px" TextMode="MultiLine" Width="800px" AutoPostBack="True"></asp:TextBox>
        </div>
        <div style="margin-left: 40px; height: 100px">
        </div>

    </form>
</body>
</html>
