﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestMultiStore
    {
        private List<string> _storeIdAry;
        private List<string> _dbNameAry;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        int TotalTxRequest = 2;
        int NumStores = 2;

        public TestMultiStore(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTxReq, int NumStores)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this.TotalTxRequest = NumTxReq;
            this.NumStores = NumStores;

           
            // initialize test stores
            this._storeIdAry = new List<string>();
            this._addStoreId(1, NumStores);
            this._dbNameAry = new List<string>();
            this._addTestDb(1, NumStores);
        }

        public TestMultiStore(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTxReq, int NumStores, List<string> StoreIdAry, List<string> StoreDbAry)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this.TotalTxRequest = NumTxReq;
            this.NumStores = NumStores;

            this._storeIdAry = new List<string>();
            foreach (string id in StoreIdAry)
                this._storeIdAry.Add(id);

            this._dbNameAry = new List<string>();
            foreach (string db in StoreDbAry)
                this._dbNameAry.Add(db);
        }

        public List<TestCounter> Start()
        {
            var list = new List<TestCounter>();
            var ravenAry = new List<RavenClient>();
            var sbAry    = new List<Scoreboard>();
            var sgAry    = new List<ScenarioGenerator>();
            var listenerAry = new List<Listener>();

            var listenTAry = new List<Thread>();
            var sgTAry = new List<Thread>();

            int [] AccessTokenAry = new int[NumStores];
            for (int i = 0; i < NumStores; i++)
                AccessTokenAry[i] = 0;

            // prepare components and test data
            for (int i = 0; i < this.NumStores; i++)
            {
                try
                {
                    ravenAry.Add(new RavenClient(_storeIdAry[i], _dumpMessage, _dbNameAry[i]));
                    //ravenAry[i].InitializeCounters();
                    // get token to access database
                    AccessTokenAry[i] = ravenAry[i].GetAccessToken();
                    if (AccessTokenAry[i] < 1)
                    {
                        _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", _storeIdAry[i]));
                        return null;
                    }
                    else
                    {
                        sbAry.Add(new Scoreboard(ravenAry[i], _dumpMessage));
                        sgAry.Add(new ScenarioGenerator(ravenAry[i], _dumpMessage));
                        listenerAry.Add(new Listener(sbAry[i], _storeIdAry[i], _dumpMessage));

                        ravenAry[i].InitializeTestData(_storeIdAry[i]);
                        ravenAry[i].Prepare(TotalTxRequest);
                        listenerAry[i].PreParse();
                    }
                }
                catch (Exception e)
                {
                    _dumpResults(string.Format("***ERROR: Database {0} error.\r\n{1}", _storeIdAry[i], e.Message));
                    return null;
                }
            }

            System.Threading.Thread.Sleep(3000);

            // start scenario generator threads
            foreach(ScenarioGenerator sg in sgAry)
            {
                var t = new Thread(() => { sg.Start(this.TotalTxRequest); });
                sgTAry.Add(t);
                t.Start();
                while (t.ThreadState != ThreadState.Running)
                    Thread.Sleep(200);
            }

            // join all scenario generator threads
            foreach(Thread t in sgTAry)
                t.Join();

            System.Threading.Thread.Sleep(this.TotalTxRequest * ASConfig.AlertInterval);

            // start listener threads
            foreach(Listener l in listenerAry)
            {
                var t = new Thread(l.Start);
                listenTAry.Add(t);
                t.Start();
                while (t.ThreadState != ThreadState.Running)
                    Thread.Sleep(200);
            }

            // join all listener threads
            foreach (Thread t in listenTAry)
                t.Join();

            // create reports
            Thread.Sleep(1000);
            for (int i = 0; i < NumStores; i++)
            {
                this._dumpResults(string.Format(" \r\nSCOREBOARD REPORT FOR STORE ID: {0}\r\n", _storeIdAry[i]));
                this._dumpResults(sbAry[i].Report(listenerAry[i].Status, ASConfig.RavenDbPerformanceDbName));
            }
            for (int i = 0; i < NumStores; i++)
            {
                if (AccessTokenAry[i] > 0)
                {
                    ravenAry[i].ReturnAccessToken();
                    list.Add(ravenAry[i].GetTestCounters());
                }
            }
            return list;
        }

        private void _addStoreId(int startIdx, int endIdx)
        {
            for (int i = startIdx; i <= endIdx; i++)
            {
                switch (i)
                {
                    case 1: this._storeIdAry.Add(ASConfig.TestMultiStoreId1); break;
                    case 2: this._storeIdAry.Add(ASConfig.TestMultiStoreId2); break;
                    case 3: this._storeIdAry.Add(ASConfig.TestMultiStoreId3); break;
                    case 4: this._storeIdAry.Add(ASConfig.TestMultiStoreId4); break;
                    case 5: this._storeIdAry.Add(ASConfig.TestMultiStoreId5); break;
                    case 6: this._storeIdAry.Add(ASConfig.TestMultiStoreId6); break;
                    case 7: this._storeIdAry.Add(ASConfig.TestMultiStoreId7); break;
                    case 8: this._storeIdAry.Add(ASConfig.TestMultiStoreId8); break;
                    case 9: this._storeIdAry.Add(ASConfig.TestMultiStoreId9); break;
                    case 10: this._storeIdAry.Add(ASConfig.TestMultiStoreId10); break;
                    case 11: this._storeIdAry.Add(ASConfig.TestMultiStoreId11); break;
                    case 12: this._storeIdAry.Add(ASConfig.TestMultiStoreId12); break;
                    case 13: this._storeIdAry.Add(ASConfig.TestMultiStoreId13); break;
                    case 14: this._storeIdAry.Add(ASConfig.TestMultiStoreId14); break;
                    case 15: this._storeIdAry.Add(ASConfig.TestMultiStoreId15); break;
                    case 16: this._storeIdAry.Add(ASConfig.TestMultiStoreId16); break;
                    case 17: this._storeIdAry.Add(ASConfig.TestMultiStoreId17); break;
                    case 18: this._storeIdAry.Add(ASConfig.TestMultiStoreId18); break;
                    case 19: this._storeIdAry.Add(ASConfig.TestMultiStoreId19); break;
                    case 20: this._storeIdAry.Add(ASConfig.TestMultiStoreId20); break;
                    case 21: this._storeIdAry.Add(ASConfig.TestMultiStoreId21); break;
                    case 22: this._storeIdAry.Add(ASConfig.TestMultiStoreId22); break;
                    case 23: this._storeIdAry.Add(ASConfig.TestMultiStoreId23); break;
                    case 24: this._storeIdAry.Add(ASConfig.TestMultiStoreId24); break;
                    case 25: this._storeIdAry.Add(ASConfig.TestMultiStoreId25); break;
                    case 26: this._storeIdAry.Add(ASConfig.TestMultiStoreId26); break;
                    case 27: this._storeIdAry.Add(ASConfig.TestMultiStoreId27); break;
                    case 28: this._storeIdAry.Add(ASConfig.TestMultiStoreId28); break;
                    case 29: this._storeIdAry.Add(ASConfig.TestMultiStoreId29); break;
                    case 30: this._storeIdAry.Add(ASConfig.TestMultiStoreId30); break;
                    case 31: this._storeIdAry.Add(ASConfig.TestMultiStoreId31); break;
                    case 32: this._storeIdAry.Add(ASConfig.TestMultiStoreId32); break;
                    case 33: this._storeIdAry.Add(ASConfig.TestMultiStoreId33); break;
                    case 34: this._storeIdAry.Add(ASConfig.TestMultiStoreId34); break;
                    case 35: this._storeIdAry.Add(ASConfig.TestMultiStoreId35); break;
                    case 36: this._storeIdAry.Add(ASConfig.TestMultiStoreId36); break;
                    case 37: this._storeIdAry.Add(ASConfig.TestMultiStoreId37); break;
                    case 38: this._storeIdAry.Add(ASConfig.TestMultiStoreId38); break;
                    case 39: this._storeIdAry.Add(ASConfig.TestMultiStoreId39); break;
                    case 40: this._storeIdAry.Add(ASConfig.TestMultiStoreId40); break;
                    case 41: this._storeIdAry.Add(ASConfig.TestMultiStoreId41); break;
                    case 42: this._storeIdAry.Add(ASConfig.TestMultiStoreId42); break;
                    case 43: this._storeIdAry.Add(ASConfig.TestMultiStoreId43); break;
                    case 44: this._storeIdAry.Add(ASConfig.TestMultiStoreId44); break;
                    case 45: this._storeIdAry.Add(ASConfig.TestMultiStoreId45); break;
                    case 46: this._storeIdAry.Add(ASConfig.TestMultiStoreId46); break;
                    case 47: this._storeIdAry.Add(ASConfig.TestMultiStoreId47); break;
                    case 48: this._storeIdAry.Add(ASConfig.TestMultiStoreId48); break;
                    case 49: this._storeIdAry.Add(ASConfig.TestMultiStoreId49); break;
                    case 50: this._storeIdAry.Add(ASConfig.TestMultiStoreId50); break;
                    case 51: this._storeIdAry.Add(ASConfig.TestMultiStoreId51); break;
                    case 52: this._storeIdAry.Add(ASConfig.TestMultiStoreId52); break;
                    case 53: this._storeIdAry.Add(ASConfig.TestMultiStoreId53); break;
                    case 54: this._storeIdAry.Add(ASConfig.TestMultiStoreId54); break;
                    case 55: this._storeIdAry.Add(ASConfig.TestMultiStoreId55); break;
                    case 56: this._storeIdAry.Add(ASConfig.TestMultiStoreId56); break;
                    case 57: this._storeIdAry.Add(ASConfig.TestMultiStoreId57); break;
                    case 58: this._storeIdAry.Add(ASConfig.TestMultiStoreId58); break;
                    case 59: this._storeIdAry.Add(ASConfig.TestMultiStoreId59); break;
                    case 60: this._storeIdAry.Add(ASConfig.TestMultiStoreId60); break;
                    case 61: this._storeIdAry.Add(ASConfig.TestMultiStoreId61); break;
                    case 62: this._storeIdAry.Add(ASConfig.TestMultiStoreId62); break;
                    case 63: this._storeIdAry.Add(ASConfig.TestMultiStoreId63); break;
                    case 64: this._storeIdAry.Add(ASConfig.TestMultiStoreId64); break;
                    case 65: this._storeIdAry.Add(ASConfig.TestMultiStoreId65); break;
                    case 66: this._storeIdAry.Add(ASConfig.TestMultiStoreId66); break;
                    case 67: this._storeIdAry.Add(ASConfig.TestMultiStoreId67); break;
                    case 68: this._storeIdAry.Add(ASConfig.TestMultiStoreId68); break;
                    case 69: this._storeIdAry.Add(ASConfig.TestMultiStoreId69); break;
                    case 70: this._storeIdAry.Add(ASConfig.TestMultiStoreId70); break;
                    case 71: this._storeIdAry.Add(ASConfig.TestMultiStoreId71); break;
                    case 72: this._storeIdAry.Add(ASConfig.TestMultiStoreId72); break;
                    case 73: this._storeIdAry.Add(ASConfig.TestMultiStoreId73); break;
                    case 74: this._storeIdAry.Add(ASConfig.TestMultiStoreId74); break;
                    case 75: this._storeIdAry.Add(ASConfig.TestMultiStoreId75); break;
                    case 76: this._storeIdAry.Add(ASConfig.TestMultiStoreId76); break;
                    case 77: this._storeIdAry.Add(ASConfig.TestMultiStoreId77); break;
                    case 78: this._storeIdAry.Add(ASConfig.TestMultiStoreId78); break;
                    case 79: this._storeIdAry.Add(ASConfig.TestMultiStoreId79); break;
                    case 80: this._storeIdAry.Add(ASConfig.TestMultiStoreId80); break;
                    case 81: this._storeIdAry.Add(ASConfig.TestMultiStoreId81); break;
                    case 82: this._storeIdAry.Add(ASConfig.TestMultiStoreId82); break;
                    case 83: this._storeIdAry.Add(ASConfig.TestMultiStoreId83); break;
                    case 84: this._storeIdAry.Add(ASConfig.TestMultiStoreId84); break;
                    case 85: this._storeIdAry.Add(ASConfig.TestMultiStoreId85); break;
                    case 86: this._storeIdAry.Add(ASConfig.TestMultiStoreId86); break;
                    case 87: this._storeIdAry.Add(ASConfig.TestMultiStoreId87); break;
                    case 88: this._storeIdAry.Add(ASConfig.TestMultiStoreId88); break;
                    case 89: this._storeIdAry.Add(ASConfig.TestMultiStoreId89); break;
                    case 90: this._storeIdAry.Add(ASConfig.TestMultiStoreId90); break;
                    case 91: this._storeIdAry.Add(ASConfig.TestMultiStoreId91); break;
                    case 92: this._storeIdAry.Add(ASConfig.TestMultiStoreId92); break;
                    case 93: this._storeIdAry.Add(ASConfig.TestMultiStoreId93); break;
                    case 94: this._storeIdAry.Add(ASConfig.TestMultiStoreId94); break;
                    case 95: this._storeIdAry.Add(ASConfig.TestMultiStoreId95); break;
                    case 96: this._storeIdAry.Add(ASConfig.TestMultiStoreId96); break;
                    case 97: this._storeIdAry.Add(ASConfig.TestMultiStoreId97); break;
                    case 98: this._storeIdAry.Add(ASConfig.TestMultiStoreId98); break;
                    case 99: this._storeIdAry.Add(ASConfig.TestMultiStoreId99); break;
                    case 100: this._storeIdAry.Add(ASConfig.TestMultiStoreId100); break;
                }
            }
        }

        private void _addTestDb(int startIdx, int endIdx)
        {
            for (int i = startIdx; i <= endIdx; i++)
            {
                switch (i)
                {
                    case 1: this._dbNameAry.Add(ASConfig.TestMultiStoreDb1); break;
                    case 2: this._dbNameAry.Add(ASConfig.TestMultiStoreDb2); break;
                    case 3: this._dbNameAry.Add(ASConfig.TestMultiStoreDb3); break;
                    case 4: this._dbNameAry.Add(ASConfig.TestMultiStoreDb4); break;
                    case 5: this._dbNameAry.Add(ASConfig.TestMultiStoreDb5); break;
                    case 6: this._dbNameAry.Add(ASConfig.TestMultiStoreDb6); break;
                    case 7: this._dbNameAry.Add(ASConfig.TestMultiStoreDb7); break;
                    case 8: this._dbNameAry.Add(ASConfig.TestMultiStoreDb8); break;
                    case 9: this._dbNameAry.Add(ASConfig.TestMultiStoreDb9); break;
                    case 10: this._dbNameAry.Add(ASConfig.TestMultiStoreDb10); break;
                    case 11: this._dbNameAry.Add(ASConfig.TestMultiStoreDb11); break;
                    case 12: this._dbNameAry.Add(ASConfig.TestMultiStoreDb12); break;
                    case 13: this._dbNameAry.Add(ASConfig.TestMultiStoreDb13); break;
                    case 14: this._dbNameAry.Add(ASConfig.TestMultiStoreDb14); break;
                    case 15: this._dbNameAry.Add(ASConfig.TestMultiStoreDb15); break;
                    case 16: this._dbNameAry.Add(ASConfig.TestMultiStoreDb16); break;
                    case 17: this._dbNameAry.Add(ASConfig.TestMultiStoreDb17); break;
                    case 18: this._dbNameAry.Add(ASConfig.TestMultiStoreDb18); break;
                    case 19: this._dbNameAry.Add(ASConfig.TestMultiStoreDb19); break;
                    case 20: this._dbNameAry.Add(ASConfig.TestMultiStoreDb20); break;
                    case 21: this._dbNameAry.Add(ASConfig.TestMultiStoreDb21); break;
                    case 22: this._dbNameAry.Add(ASConfig.TestMultiStoreDb22); break;
                    case 23: this._dbNameAry.Add(ASConfig.TestMultiStoreDb23); break;
                    case 24: this._dbNameAry.Add(ASConfig.TestMultiStoreDb24); break;
                    case 25: this._dbNameAry.Add(ASConfig.TestMultiStoreDb25); break;
                    case 26: this._dbNameAry.Add(ASConfig.TestMultiStoreDb26); break;
                    case 27: this._dbNameAry.Add(ASConfig.TestMultiStoreDb27); break;
                    case 28: this._dbNameAry.Add(ASConfig.TestMultiStoreDb28); break;
                    case 29: this._dbNameAry.Add(ASConfig.TestMultiStoreDb29); break;
                    case 30: this._dbNameAry.Add(ASConfig.TestMultiStoreDb30); break;
                    case 31: this._dbNameAry.Add(ASConfig.TestMultiStoreDb31); break;
                    case 32: this._dbNameAry.Add(ASConfig.TestMultiStoreDb32); break;
                    case 33: this._dbNameAry.Add(ASConfig.TestMultiStoreDb33); break;
                    case 34: this._dbNameAry.Add(ASConfig.TestMultiStoreDb34); break;
                    case 35: this._dbNameAry.Add(ASConfig.TestMultiStoreDb35); break;
                    case 36: this._dbNameAry.Add(ASConfig.TestMultiStoreDb36); break;
                    case 37: this._dbNameAry.Add(ASConfig.TestMultiStoreDb37); break;
                    case 38: this._dbNameAry.Add(ASConfig.TestMultiStoreDb38); break;
                    case 39: this._dbNameAry.Add(ASConfig.TestMultiStoreDb39); break;
                    case 40: this._dbNameAry.Add(ASConfig.TestMultiStoreDb40); break;
                    case 41: this._dbNameAry.Add(ASConfig.TestMultiStoreDb41); break;
                    case 42: this._dbNameAry.Add(ASConfig.TestMultiStoreDb42); break;
                    case 43: this._dbNameAry.Add(ASConfig.TestMultiStoreDb43); break;
                    case 44: this._dbNameAry.Add(ASConfig.TestMultiStoreDb44); break;
                    case 45: this._dbNameAry.Add(ASConfig.TestMultiStoreDb45); break;
                    case 46: this._dbNameAry.Add(ASConfig.TestMultiStoreDb46); break;
                    case 47: this._dbNameAry.Add(ASConfig.TestMultiStoreDb47); break;
                    case 48: this._dbNameAry.Add(ASConfig.TestMultiStoreDb48); break;
                    case 49: this._dbNameAry.Add(ASConfig.TestMultiStoreDb49); break;
                    case 50: this._dbNameAry.Add(ASConfig.TestMultiStoreDb50); break;
                    case 51: this._dbNameAry.Add(ASConfig.TestMultiStoreDb51); break;
                    case 52: this._dbNameAry.Add(ASConfig.TestMultiStoreDb52); break;
                    case 53: this._dbNameAry.Add(ASConfig.TestMultiStoreDb53); break;
                    case 54: this._dbNameAry.Add(ASConfig.TestMultiStoreDb54); break;
                    case 55: this._dbNameAry.Add(ASConfig.TestMultiStoreDb55); break;
                    case 56: this._dbNameAry.Add(ASConfig.TestMultiStoreDb56); break;
                    case 57: this._dbNameAry.Add(ASConfig.TestMultiStoreDb57); break;
                    case 58: this._dbNameAry.Add(ASConfig.TestMultiStoreDb58); break;
                    case 59: this._dbNameAry.Add(ASConfig.TestMultiStoreDb59); break;
                    case 60: this._dbNameAry.Add(ASConfig.TestMultiStoreDb60); break;
                    case 61: this._dbNameAry.Add(ASConfig.TestMultiStoreDb61); break;
                    case 62: this._dbNameAry.Add(ASConfig.TestMultiStoreDb62); break;
                    case 63: this._dbNameAry.Add(ASConfig.TestMultiStoreDb63); break;
                    case 64: this._dbNameAry.Add(ASConfig.TestMultiStoreDb64); break;
                    case 65: this._dbNameAry.Add(ASConfig.TestMultiStoreDb65); break;
                    case 66: this._dbNameAry.Add(ASConfig.TestMultiStoreDb66); break;
                    case 67: this._dbNameAry.Add(ASConfig.TestMultiStoreDb67); break;
                    case 68: this._dbNameAry.Add(ASConfig.TestMultiStoreDb68); break;
                    case 69: this._dbNameAry.Add(ASConfig.TestMultiStoreDb69); break;
                    case 70: this._dbNameAry.Add(ASConfig.TestMultiStoreDb70); break;
                    case 71: this._dbNameAry.Add(ASConfig.TestMultiStoreDb71); break;
                    case 72: this._dbNameAry.Add(ASConfig.TestMultiStoreDb72); break;
                    case 73: this._dbNameAry.Add(ASConfig.TestMultiStoreDb73); break;
                    case 74: this._dbNameAry.Add(ASConfig.TestMultiStoreDb74); break;
                    case 75: this._dbNameAry.Add(ASConfig.TestMultiStoreDb75); break;
                    case 76: this._dbNameAry.Add(ASConfig.TestMultiStoreDb76); break;
                    case 77: this._dbNameAry.Add(ASConfig.TestMultiStoreDb77); break;
                    case 78: this._dbNameAry.Add(ASConfig.TestMultiStoreDb78); break;
                    case 79: this._dbNameAry.Add(ASConfig.TestMultiStoreDb79); break;
                    case 80: this._dbNameAry.Add(ASConfig.TestMultiStoreDb80); break;
                    case 81: this._dbNameAry.Add(ASConfig.TestMultiStoreDb81); break;
                    case 82: this._dbNameAry.Add(ASConfig.TestMultiStoreDb82); break;
                    case 83: this._dbNameAry.Add(ASConfig.TestMultiStoreDb83); break;
                    case 84: this._dbNameAry.Add(ASConfig.TestMultiStoreDb84); break;
                    case 85: this._dbNameAry.Add(ASConfig.TestMultiStoreDb85); break;
                    case 86: this._dbNameAry.Add(ASConfig.TestMultiStoreDb86); break;
                    case 87: this._dbNameAry.Add(ASConfig.TestMultiStoreDb87); break;
                    case 88: this._dbNameAry.Add(ASConfig.TestMultiStoreDb88); break;
                    case 89: this._dbNameAry.Add(ASConfig.TestMultiStoreDb89); break;
                    case 90: this._dbNameAry.Add(ASConfig.TestMultiStoreDb90); break;
                    case 91: this._dbNameAry.Add(ASConfig.TestMultiStoreDb91); break;
                    case 92: this._dbNameAry.Add(ASConfig.TestMultiStoreDb92); break;
                    case 93: this._dbNameAry.Add(ASConfig.TestMultiStoreDb93); break;
                    case 94: this._dbNameAry.Add(ASConfig.TestMultiStoreDb94); break;
                    case 95: this._dbNameAry.Add(ASConfig.TestMultiStoreDb95); break;
                    case 96: this._dbNameAry.Add(ASConfig.TestMultiStoreDb96); break;
                    case 97: this._dbNameAry.Add(ASConfig.TestMultiStoreDb97); break;
                    case 98: this._dbNameAry.Add(ASConfig.TestMultiStoreDb98); break;
                    case 99: this._dbNameAry.Add(ASConfig.TestMultiStoreDb99); break;
                    case 100: this._dbNameAry.Add(ASConfig.TestMultiStoreDb100); break;
                }
            }
        }
    }
}