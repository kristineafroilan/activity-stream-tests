﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestDeactivatedAccount
    {
        private string _dbName = ASConfig.TestDeactivatedAccountDb;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        private int _numTx;

        public TestDeactivatedAccount(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx, string DbName)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numTx = NumTx;
            this._dbName = DbName;
        }

        public TestDeactivatedAccount(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numTx = NumTx;
        }

        public List<TestCounter> Start()
        {
            var list = new List<TestCounter>();

            try
            {
                var stores = AmazonSqlClient.FetchDeactivatedStores();

                foreach (Merchant store in stores)
                {
                    if (store.Email.StartsWith("qa+cancelled"))
                    {
                        list.Add(_sendIgnored(store.StoreId, this._numTx));
                    }
                }
                
            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }
            return list;
        }

        private TestCounter _sendIgnored(string StoreId, int NumTx)
        {
            // instantiate components
            var raven = new RavenClient(StoreId, _dumpMessage, _dbName);
            raven.SetTimeout(600000000);
            //_create(raven, NumTx, StoreId);
            var sb = new Scoreboard(raven, _dumpMessage);
            var sg = new ScenarioGenerator(raven, _dumpMessage);
            var listener = new Listener(sb, StoreId, _dumpMessage);

            // get access token from database
            if (raven.GetAccessToken() < 1)
            {
                _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", StoreId));
                return null;
            }

            // reset test data
            raven.InitializeTestData(StoreId);
            raven.PrepareClusteredDataRequest(StoreId, raven.GetCurrentTxId(), NumTx, true);

            // create threads
            Thread listenT = new Thread(listener.Start);
            Thread timeoutT = new Thread(listener.IsAliveChecker);

            // pre-start listener to get timestamps
            listener.PreParse();
            System.Threading.Thread.Sleep(3000);

            // start
            if(!sg.Start(1))
                return null;
            listenT.Start();

            // wait for listener thread to stop
            listenT.Join();

            // create reports
            Thread.Sleep(1000);
            this._dumpResults(sb.Report(listener.Status, ASConfig.RavenDbPerformanceDbName, 0, true));

            // return access token
            raven.ReturnAccessToken();
            Thread.Sleep(1000);
            return raven.GetTestCounters();
        }
    }
}