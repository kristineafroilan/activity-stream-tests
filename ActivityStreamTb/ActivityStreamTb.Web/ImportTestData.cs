﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ActivityStreamTb.Web
{
    public class ImportTestData
    {
        public void ImportFile(string DbName, String Source, LoadDumpDelegate results)
        {
            string content;

            try
            {
                using (var reader = new System.IO.StreamReader(Source))
                {
                    content = reader.ReadToEnd();
                }
                string[] orders = content.Trim().Split(new string[] { "\n" }, StringSplitOptions.None);
                foreach (string s in orders)
                {
                    results(string.Format("string:: {0} \r\n", s));
                    var order = JsonConvert.DeserializeObject<Order>(s);
                    results(order.Print());
                    var raven = new RavenClient(order.StoreId, results, DbName);
                    raven.CreateOrder(order);
                }
            }

            catch (Exception e)
            {
                results(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }
        }

        public void Import(string DbName, HttpPostedFile Source, LoadDumpDelegate results)
        {
            string content;

            try
            {
                using (var reader = new System.IO.StreamReader(Source.InputStream))
                {
                    content = reader.ReadToEnd();
                }
                string[] orders = content.Trim().Split(new string[] { "\n" }, StringSplitOptions.None);
                foreach (string s in orders)
                {
                    results(string.Format("string:: {0} \r\n", s));
                    var order = JsonConvert.DeserializeObject<Order>(s);
                    results(order.Print());
                    var raven = new RavenClient(order.StoreId, results, DbName);
                    raven.CreateOrder(order);
                }
            }

            catch (Exception e)
            {
                results(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }
        }
    }
}