﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityStreamTb.Web
{
    public class TestAdminReportAlertsPerCustomer
    {
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        public TestAdminReportAlertsPerCustomer(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
        }

        public string Start()
        {
            string summary = string.Format("\r\n{0}", Merchant.PrintHeader());
            System.Console.WriteLine(summary);

            try
            {
                var stores = AmazonSqlClient.FetchSubscriptions();

                stores = stores.OrderBy(o => (o.FirstName + " " + o.LastName)).ToList();

                int totalAlertsServed = 0;
                int totalAlertsViewed = 0;
                int totalNotPublished = 0;

                foreach (Merchant store in stores)
                {
                    var listener = new Listener(this._dumpMessage);
                    store.CurrentUsage = listener.CountAlertSent(store);
                    store.AlertsViewed = listener.CountUniqueWidget(store.StoreId);
                    store.NotPublished = listener.CountNotPublished(store.StoreId);
                    summary = string.Format("{0}\r\n{1}", summary, store.PrintAll());
                    System.Console.WriteLine(store.PrintAll());

                    totalAlertsServed += store.CurrentUsage;
                    totalAlertsViewed += store.AlertsViewed;
                    totalNotPublished += store.NotPublished;
                }

                summary = string.Format("{0}\r\n\r\n\tTotal Customers     = {1}", summary, stores.Count());
                summary = string.Format("{0}\r\n\tTotal Alerts Served = {1}", summary, totalAlertsServed);
                summary = string.Format("{0}\r\n\tTotal Alerts Viewed = {1}", summary, totalAlertsViewed);
                summary = string.Format("{0}\r\n\tTotal Not Published = {1}\r\n", summary, totalNotPublished);
            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Exception encountered.\r\n{0}", e.Message));
            }

            return summary;
        }
    }
}