﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityStreamTb.Web
{
    public class TestAlertSentCounter
    {
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        public TestAlertSentCounter(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
        }

        public string Start()
        {
            string result = "";
            try
            {
                var stores = AmazonSqlClient.FetchActiveStores();
                _dumpResults(" \r\nCounter for Alert Sent per Store");
                foreach (Merchant store in stores)
                {
                    var listener = new Listener(this._dumpMessage);
                    store.CurrentUsage = listener.CountAlertSent(store);
                    _dumpResults(string.Format("{0}", store.Print()));
                    if (store.CurrentUsage > store.AlertCap)
                    {
                        _dumpResults(string.Format("{0}\r\n***ERROR: Usage > Alert Cap! \r\n{1}", result, store.Print()));
                        result = string.Format("{0}\r\n***ERROR: Usage > Alert Cap! \r\n{1}", result, store.Print());
                    }
                    if (store.CurrentUsage >= store.AlertCap && !store.IsLimitReached)
                    {
                        _dumpResults(string.Format("{0}\r\n***ERROR: LimitReached set to false! \r\n{1}", result, store.Print()));
                        result = string.Format("{0}\r\n***ERROR: LimitReached set to false", result, store.Print());
                    }
                }
            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Exception encountered.\r\n{0}", e.Message));
            }
            return result;
        }
    }
}