﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ActivityStreamTb.Web
{
    public class OrderQueuedEventItems
    {
        public List<OrderQueuedEvent> Items { get; set; }
    }

    public class OrderQueuedEvent
    {
        public string FirstName { get; set; }
        public string Item { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }

        public Order ToOrder(string storeId)
        {
            Order to = new Order();
            to.StoreId = storeId;
            to.IsQueued = true;
            to.QueuedTimestamp = this.Date;
            to.FirstName = this.FirstName;
            to.Item = this.Item;
            to.Quantity = this.Quantity;
            to.Price = this.Price;
            to.StoreUrl = this.Url;
            to.FormMessages();
            return to;
        }
    }

    public class NotificationSentEventItems
    {
        public List<NotificationSentEvent> Items { get; set; }
    }

    public class NotificationSentEvent
    {
        public string FirstName { get; set; }
        public string Item { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }
        public int Priority { get; set; }
        
        public Order ToOrder(string storeId)
        {
            Order to = new Order();
            to.StoreId = storeId;
            to.IsBroadcasted = true;
            to.BroadcastedTimestamp = this.Date;
            to.FirstName = this.FirstName;
            to.Item = this.Item;
            to.Quantity = this.Quantity;
            to.Price = this.Price;
            to.StoreUrl = this.Url;
            to.FormMessages();
            return to;
        }
    }

    public class NotificationReceivedEventItems
    {
        public List<NotificationReceivedEvent> Items { get; set; }
    }

    public class NotificationReceivedEvent
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string UserCookie { get; set; }

        public Order ToOrder(string storeId)
        {
            Order to = new Order();
            to.StoreId = storeId;
            to.IsViewed = true;
            to.ViewedTimestamp = this.Date;
            to.WidgetMessage = this.Message;
            return to;
        }

        public NotificationReceivedEvent CopyTo()
        {
            var obj = new NotificationReceivedEvent();
            obj.Date = this.Date;
            obj.Message = this.Message;
            obj.UserCookie = this.UserCookie;
            return obj;
        }
    }

    public class NotPublishedEventItems
    {
        public List<NotPublishedEvent> Items { get; set; }
    }

    public class NotPublishedEvent
    {
        public string FirstName { get; set; }
        public string Item { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }

        public Order ToOrder(string storeId)
        {
            Order to = new Order();
            to.StoreId = storeId;
            to.IsUnpublished = true;
            to.FirstName = this.FirstName;
            to.Item = this.Item;
            to.Quantity = this.Quantity;
            to.Price = this.Price;
            to.StoreUrl = this.Url;
            to.FormMessages();
            return to;
        }
    }
}
