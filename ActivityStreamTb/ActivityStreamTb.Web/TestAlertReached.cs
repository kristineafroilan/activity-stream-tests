﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestAlertReached
    {
        private string _dbName = ASConfig.TestAlertReachedDb;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        // global
        int _numtx;

        public TestAlertReached(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx, string DbName)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numtx = NumTx;
            this._dbName = DbName;
        }

        public TestAlertReached(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numtx = NumTx;
        }

        public List<TestCounter> Start()
        {
            var list = new List<TestCounter>();

            try
            {
                var stores = AmazonSqlClient.FetchActiveStores();

                foreach (Merchant store in stores)
                {
                    var listener = new Listener(this._dumpMessage);
                    store.CurrentUsage = listener.CountAlertSent(store);
                    if (store.CurrentUsage > store.AlertCap)
                        _dumpResults(string.Format("***ERROR: Able to send requests past Alert cap!!! Usage = {0} :: Cap = {1}", store.CurrentUsage, store.AlertCap));

                    if(store.CurrentUsage >= store.AlertCap)
                        list.Add(_sendUnpublished(store.StoreId, this._numtx));
                }

                if(list.Count() < 1)
                    _dumpResults("***WARNING: Test Failed! No merchant with alert cap reached!");

            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }

            return list;
        }

        private TestCounter _sendUnpublished(string StoreId, int NumTx)
        {
            // instantiate components
            var raven = new RavenClient(StoreId, _dumpMessage, _dbName);
            //_create(raven, NumTx, StoreId);
            var sb = new Scoreboard(raven, _dumpMessage);
            var sg = new ScenarioGenerator(raven, _dumpMessage);
            var listener = new Listener(sb, StoreId, _dumpMessage);

            // get access token from database
            if (raven.GetAccessToken() < 1)
            {
                _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", StoreId));
                return null;
            }

            // reset test data
            raven.InitializeTestData(StoreId);
            raven.PrepareClusteredDataRequest(StoreId, raven.GetCurrentTxId(), NumTx);

            // create threads
            Thread listenT = new Thread(listener.Start);
            Thread timeoutT = new Thread(listener.IsAliveChecker);

            // pre-start listener to get timestamps
            listener.PreParse();
            System.Threading.Thread.Sleep(3000);

            // start
            sg.Start(1);
            listenT.Start();

            // wait for listener thread to stop
            listenT.Join();

            // create reports
            Thread.Sleep(1000);
            this._dumpResults(sb.Report(listener.Status, ASConfig.RavenDbPerformanceDbName, NumTx));

            // return access token
            raven.ReturnAccessToken();
            Thread.Sleep(1000);
            return raven.GetTestCounters();
        }
    }
}