﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityStreamTb.Web
{
    public class PerformanceCounter
    {
        public DateTime totalSentQueuedTime { get; set; }
        public DateTime totalQueuedBroadcastedTime { get; set; }
        public DateTime totalBroadcastedViewedTime { get; set; }
        public DateTime totalTime { get; set; }
        public int totalViews { get; set; }
        public int totalCount { get; set; }

        public PerformanceCounter()
        {
            this.totalSentQueuedTime = new DateTime();
            this.totalQueuedBroadcastedTime = new DateTime();
            this.totalBroadcastedViewedTime = new DateTime();
            this.totalTime = new DateTime();
            this.totalViews = 0;
            this.totalCount = 0;            
        }

        public void Add(PerformanceCounter p)
        {
            this.totalSentQueuedTime = this.totalSentQueuedTime.Add(new TimeSpan(p.totalSentQueuedTime.Ticks));
            this.totalQueuedBroadcastedTime = this.totalQueuedBroadcastedTime.Add(new TimeSpan(p.totalQueuedBroadcastedTime.Ticks));
            this.totalBroadcastedViewedTime = this.totalBroadcastedViewedTime.Add(new TimeSpan(p.totalBroadcastedViewedTime.Ticks));
            this.totalTime = this.totalTime.Add(new TimeSpan(p.totalTime.Ticks));
            this.totalViews += p.totalViews;
            this.totalCount += p.totalCount;
        }

        public void AddSentQueuedTime(TimeSpan SentQueuedSpan)
        {
            this.totalSentQueuedTime = this.totalSentQueuedTime.Add(SentQueuedSpan);
        }

        public void AddQueuedBroadcastedTime(TimeSpan QueuedBroadcastedSpan)
        {
            this.totalQueuedBroadcastedTime = this.totalQueuedBroadcastedTime.Add(QueuedBroadcastedSpan);
        }

        public void AddBroadcastedViewedTime(TimeSpan BroadcastedViewedSpan)
        {
            this.totalBroadcastedViewedTime = this.totalBroadcastedViewedTime.Add(BroadcastedViewedSpan);
        }

        public void AddTotalTime(TimeSpan TotalSpan)
        {
            this.totalTime = this.totalTime.Add(TotalSpan);
        }

        public void AddViewCount(int ViewCount)
        {
            this.totalViews += ViewCount;
        }

        public string GetPerformanceReport()
        {
            if (totalCount > 0)
            {
                string str = " \r\n----------------------------------------------------------------";
                str = string.Format("{0} \r\n---------------------- Performance Report ----------------------", str);
                str = string.Format("{0} \r\n----------------------------------------------------------------", str);
                str = string.Format("{0} \r\nAverage Rate: AS API -> Queuer           : {1} sec/order", str, (new TimeSpan(totalSentQueuedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Queuer -> Broadcaster      : {1} sec/order", str, (new TimeSpan(totalQueuedBroadcastedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Broadcaster -> Viewer      : {1} sec/order", str, (new TimeSpan(totalBroadcastedViewedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Data Request -> Broadcaster: {1} sec/order", str, (new TimeSpan(totalTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage view per order                   : {1} view(s)", str, (totalViews / totalCount).ToString());
                str = string.Format("{0} \r\nTotal successful orders                  : {1} view(s)", str, totalCount.ToString());
                str = string.Format("{0} \r\n---------------------------------------------------------------- \r\n", str);
                return str;
            }
            return null;
        }

        public string GetOverallReport()
        {
            if (totalCount > 0)
            {
                string str = "====================================================================";
                str = string.Format("{0} \r\n-------------------- OVERALL PERFORMANCE REPORT --------------------", str);
                str = string.Format("{0} \r\n====================================================================", str);
                str = string.Format("{0} \r\nAverage Rate: AS API -> Queuer           : {1} sec/order", str, (new TimeSpan(totalSentQueuedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Queuer -> Broadcaster      : {1} sec/order", str, (new TimeSpan(totalQueuedBroadcastedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Broadcaster -> Viewer      : {1} sec/order", str, (new TimeSpan(totalBroadcastedViewedTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage Rate: Data Request -> Broadcaster: {1} sec/order", str, (new TimeSpan(totalTime.Ticks).TotalSeconds / totalCount).ToString());
                str = string.Format("{0} \r\nAverage view per order                   : {1} view(s)", str, (totalViews / totalCount).ToString());
                str = string.Format("{0} \r\nTotal successful orders                  : {1} view(s)", str, totalCount.ToString());
                str = string.Format("{0} \r\n====================================================================\r\n", str);
                return str;
            }
            return null;
        }
    }
}