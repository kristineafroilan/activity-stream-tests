﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace ActivityStreamTb.Web
{
    public class TestBufferOverrun
    {
        private string _storeId = ASConfig.TestBufferOverrunId;
        private string _dbName = ASConfig.TestBufferOverrunDb;
        private LoadDumpDelegate _dumpMessage;
        private LoadDumpDelegate _dumpResults;

        private int _numTx;

        public TestBufferOverrun(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx, string StoreId, string DbName)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._storeId = StoreId;
            this._dbName = DbName;
            this._numTx = NumTx;
        }

        public TestBufferOverrun(LoadDumpDelegate DumpMessage, LoadDumpDelegate DumpResults, int NumTx)
        {
            this._dumpMessage = DumpMessage;
            this._dumpResults = DumpResults;
            this._numTx = NumTx;
        }

        public TestCounter Start()
        {
            var raven = new RavenClient(_storeId, _dumpMessage, _dbName);
            if (raven.GetAccessToken() < 1)
            {
                _dumpResults(string.Format("***ERROR: {0} database is currently busy. Please try again later.", _storeId));
                return null;
            }

            try
            {
                _sendRequests(raven, _storeId, this._numTx);
            }
            catch (Exception e)
            {
                _dumpResults(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
            }

            raven.ReturnAccessToken();
            return raven.GetTestCounters();
        }

        private void _sendRequests(RavenClient raven, string StoreId, int numtx)
        {
            // test data
            //_create(raven, this._numTx, StoreId);
            raven.InitializeTestData(StoreId);
            raven.PrepareClusteredDataRequest(StoreId, raven.GetCurrentTxId(), numtx);
            
            // instantiate components
            var sb = new Scoreboard(raven, _dumpMessage);
            var sg = new ScenarioGenerator(raven, _dumpMessage);
            var listener = new Listener(sb, StoreId, _dumpMessage);

            // create threads
            Thread listenT = new Thread(listener.Start);
            Thread timeoutT = new Thread(listener.IsAliveChecker);

            // pre-start listener to get timestamps
            listener.PreParse();
            System.Threading.Thread.Sleep(3000);

            // start
            //sg.Start(1);
            //listenT.Start();

            // wait for listener thread to stop
            //listenT.Join();

            // create reports
            Thread.Sleep(1000);
            this._dumpResults(sb.Report(listener.Status, ASConfig.RavenDbPerformanceDbName, numtx));
        }

        private void _create(RavenClient raven, int numtx, string storeid)
        {
            for (int i = 0; i < numtx; i++)
            {
                var o = new Order()
                {
                    TxId = i.ToString(),
                    StoreId = storeid,
                    FirstName = string.Format("First Name {0}", i),
                    Item = string.Format("Buffer Overrun Item {0}", i),
                    Quantity = i,
                    Price = i.ToString()
                };
                raven.CreateOrder(o);
            }
        }
    }
}