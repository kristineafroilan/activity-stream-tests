﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raven.Client.Document;

namespace ActivityStreamTb.Web
{
    public class RavenClient
    {
        private DateTime _lastTx;

        private DocumentStore _docStore;
        private int _currentTxId = 0;
        private string _fetchStoreID;
        private LoadDumpDelegate _dumpMessage;

        private string _dbName;
        private TimeSpan Timeout = ASConfig.Timeout;

        // counters
        private TestCounter _counter = new TestCounter();

        public RavenClient(string FetchStoreId, LoadDumpDelegate DumpMessage, string DbName)
        {
            this._docStore = new DocumentStore { Url = ASConfig.RavenDbUrl, ApiKey = ASConfig.RavenDbApiKey};
            this._docStore.Conventions.MaxNumberOfRequestsPerSession = ASConfig.RavenDbMaxNumReqPerSession;
            this._docStore.Initialize();

            this._currentTxId = 0;

            this._counter = new TestCounter();
            this._counter.StoreId = FetchStoreId;

            this._updateTimeoutChecker();

            this._fetchStoreID = FetchStoreId;
            this._dumpMessage = DumpMessage;
            this._dbName = DbName;
        }

        private void _updateTimeoutChecker()
        {
            // update timeout checker
            this._lastTx = DateTime.UtcNow;
        }

        public int GetAccessToken()
        {
            int token = 0;
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                if (stat.IsIdle)
                    token = 1;
                stat.IsIdle = false;

                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.GetAccessToken()::Failed to request for access token.\r\n{0}", e.Message));
                return -1;
            }
            return token;
        }

        public void ReturnAccessToken()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                stat.IsIdle = true;
                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.ReturnAccessToken()::Failed to return access token.\r\n{0}", e.Message));
            }
        }

        // unclustered test data
        public void InitializeTestData(string StoreId)
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = session.Query<Order>().ToList<Order>();

                int i = 0;
                foreach (Order r in results)
                {
                    switch (i % 5)
                    {
                        //case 0: r.Currency = "$"; break;
                        //case 1: r.Currency = "£"; break;
                        //case 2: r.Currency = "€"; break;
                        //case 3: r.Currency = "؋"; break;
                        //case 4: r.Currency = "¥"; break;
                        case 0: r.Currency = "$"; break;
                        case 1: r.Currency = "$"; break;
                        case 2: r.Currency = "$"; break;
                        case 3: r.Currency = "$"; break;
                        case 4: r.Currency = "$"; break;
                    }

                    r.FormMessages();
                    r.TxId = i.ToString();
                    r.Reset();
                    r.StoreId = StoreId;

                    i += 1;
                }

                session.SaveChanges();
                session.Dispose();

                this._updateTimeoutChecker();
                _dumpMessage("***DEBUG: RavenClient.InitializeTestData()::Test data initialized.");
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.InitializeTestData()::Failed to initialize test data.\r\n{0}", e.Message));
            }
        }

        // store-specific
        public void Prepare(int NumTxRequest)
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);

                for (int i = 0; i < NumTxRequest; i++)
                {
                    var results = session.Query<Order>().ToList<Order>();
                    
                    foreach (Order r in results)
                    {
                        var txId = (this._currentTxId + i).ToString();                    
                        if (r.StoreId.Equals(this._fetchStoreID) && 
                            r.TxId.Equals(txId))
                        {
                            r.Reset();
                            r.IsExpected = true;
                            r.ViewCount = 0;
                            this._counter.TotalPrepared += 1;
                        }
                    }
                }

                session.SaveChanges();
                session.Dispose();

                this._updateTimeoutChecker();
                _dumpMessage("***DEBUG: RavenClient.Prepare()::Test data prepared.");
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.Prepare()::Failed to prepare data request.\r\n{0}", e.Message));
            }
        }

        // store-specific methods
        public List<Order> Fetch()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = session.Query<Order>().ToList<Order>();

                var list = new List<Order>();

                foreach (Order r in results)
                {
                    if (r.TxId.Equals(this._currentTxId.ToString()) && 
                        r.StoreId.Equals(this._fetchStoreID) &&
                        r.IsExpected)
                        list.Add(r.CopyTo());
                }

                session.Dispose();
                // increment Transaction Id
                this._currentTxId += 1;
                this._updateTimeoutChecker();
                return list;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.Fetch()::Failed to fetch data request.\r\n{0}", e.Message));
            }

            return null;
        }

        public void UpdatePreparedOrder(Order source)
        {            
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = session.Query<Order>().ToList<Order>();
                foreach (Order r in results)
                {
                    if (r.IsExpected &&
                        r.TxId.Equals(source.TxId) &&
                        r.StoreId.Equals(source.StoreId) &&
                        r.FirstName.Equals(source.FirstName) &&
                        r.Item.Equals(source.Item) &&
                        r.Quantity.Equals(source.Quantity) &&
                        r.Price.Equals(source.Price))
                    {
                        r.SoftUpdate(source);
                        this._counter.TotalSent += 1;
                        break;
                    }
                }
                session.SaveChanges();
                session.Dispose();
                this._updateTimeoutChecker();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.UpdateOrder()::Failed to update order.\r\n{0}", e.Message));
            }
        }

        public void CreateOrder(Order o)
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                session.Store(o);
                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.CreateOrder()::Failed to create order.\r\n{0}", e.Message));
            }
        }

        public bool UpdateSentOrder(Order o)
        {
            var success = false;
            var session = this._docStore.OpenSession(this._dbName);

            try
            {
                var results = session.Query<Order>().ToList<Order>();
                
                // queued
                if (o.IsQueued)
                {
                    foreach(Order r in results)
                    {
                        if (r.OrderQueuedMessage.Equals(o.OrderQueuedMessage, StringComparison.OrdinalIgnoreCase) &&
                            r.IsExpected &&
                            r.IsSent &&
                            !r.IsQueued &&
                            !r.IsBroadcasted &&
                            !r.IsViewed)
                        {
                            r.IsQueued = true;
                            r.QueuedTimestamp = o.QueuedTimestamp;
                            session.SaveChanges();
                            success = true;

                            this._counter.TotalQueued += 1;
                            // update stats
                            //var counter = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                            //counter.TotalQueued += 1;
                            //session.SaveChanges();
                            break;
                        }
                    }
                }

                // broadcasted
                else if (o.IsBroadcasted)
                {
                    foreach(Order r in results)
                    {
                        if (r.NotificationSentMessage.Equals(o.NotificationSentMessage, StringComparison.OrdinalIgnoreCase) &&
                            r.IsExpected &&
                            r.IsSent &&
                            r.IsQueued &&
                            !r.IsBroadcasted)
//                            !r.IsBroadcasted &&
//                            !r.IsViewed)
                        {
                            r.IsBroadcasted = true;
                            r.BroadcastedTimestamp = o.BroadcastedTimestamp;
                            session.SaveChanges();
                            success = true;

                            this._counter.TotalBroadcasted += 1;

                            // update stats
                            //var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                            //stat.TotalBroadcasted += 1;
                            //session.SaveChanges();
                            break;
                        }
                    }
                }

                // viewed: use widget message
                else if(o.IsViewed)
                {
                    //var isNew = false;
                    foreach(Order r in results)
                    {
                        if (r.WidgetMessage.Equals(o.WidgetMessage, StringComparison.OrdinalIgnoreCase) &&
                            r.IsExpected &&
                            r.IsSent &&
                            r.IsQueued)
//                            r.IsQueued &&
//                            r.IsBroadcasted)
                        {
                            if (!r.IsViewed)
                            {
                                //isNew = true;
                                r.IsViewed = true;
                                this._counter.TotalViewed += 1;
                            }
                            r.ViewCount += 1;
                            r.ViewedTimestamp = o.ViewedTimestamp;
                            session.SaveChanges();
                            success = true;
                            //if (isNew)
                            //{
                            //    var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                            //    stat.TotalViewed += 1;
                            //    session.SaveChanges();
                            //}
                            break;
                        }
                    }
                }
                else if (o.IsUnpublished)
                {
                    foreach (Order r in results)
                    {
                        if (r.NotPublishedMessage.Equals(o.NotPublishedMessage, StringComparison.OrdinalIgnoreCase) &&
                            r.IsExpected &&
                            r.IsSent &&
                            !r.IsQueued)
                        {
                            r.IsUnpublished = true;
                            session.SaveChanges();
                            success = true;
                            this._counter.TotalUnpublished += 1;

                            // update stats
                            //var counter = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                            //counter.TotalUnpublished += 1;
                            //session.SaveChanges();
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.UpdateSentOrder()::Failed to update order.\r\n{0}\r\n{1}", e.Message, o.Print()));
            }
            session.Dispose();
            this._updateTimeoutChecker();
            return success;
        }

        private List<Order> GetCopyOfResults(List<Order> list)
        {
            var cp = new List<Order>();
            foreach (Order o in list)
                cp.Add(o.CopyTo());
            return cp;
        }

        public List<Order> FetchUnsentOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected && !r.IsSent)
                               select r).ToList<Order>();

                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchUnsentOrders()::Failed to fetch unsent orders.\r\n{0}", e.Message));
            }
            return null;
        }

        public List<Order> FetchUnqueuedOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected &&
                               r.IsSent &&
                               !r.IsQueued &&
                               !r.IsUnpublished &&
                               !r.IsExpIgnored)
                               select r).ToList<Order>();

                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchUnqueuedOrders()::Failed to fetch unqueued orders.\r\n{0}", e.Message));
            }
            return null;
        }


        public List<Order> FetchLostOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected &&
                               r.IsSent &&
                               r.IsQueued &&
                               !r.IsBroadcasted)
                               select r).ToList<Order>();
                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();

                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchLostOrders()::Failed to fetch unbroadcasted orders.\r\n{0}", e.Message));
            }
            return null;
        }

        public List<Order> FetchNotPublishedOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected && r.IsUnpublished)
                               select r).ToList<Order>();

                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchNotPublishedOrders()::Failed to fetch unpublished orders.\r\n{0}", e.Message));
            }
            return null;
        }

        // successful: sent, broadcasted, viewed
        public List<Order> FetchSuccessfulOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected &&
                               r.IsSent &&
                               r.IsQueued &&
                               r.IsBroadcasted &&
                               r.IsViewed)
                               select r).ToList<Order>();
                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchSuccessfulOrders()::Failed to fetch successful orders.\r\n{0}", e.Message));
            }
            return null;
        }

        public List<Order> FetchUnViewedOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                session.Advanced.MaxNumberOfRequestsPerSession = 100;
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected &&
                               r.IsSent &&
                               r.IsQueued &&
                               r.IsBroadcasted &&
                               !r.IsViewed)
                               select r).ToList<Order>();
                // get copy of results
                var data = GetCopyOfResults(results);

                session.Dispose();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchUnviewedOrders()::Failed to fetch unviewed orders.\r\n{0}", e.Message));
            }
            return null;
        }

        public void CheckCompleteness(ListenerStopperDelegate stopper)
        {
            if (this._counter.TotalPrepared == this._counter.TotalBroadcasted + this._counter.TotalUnpublished)
                stopper(TestStatus.SUCCESS);
        }

        public void SetTimeout(int ticks)
        {
            this.Timeout = new TimeSpan(ticks);
        }

        public void CheckTimeout(ListenerStopperDelegate stopper)
        {
            if (DateTime.UtcNow.Subtract(this._lastTx) > this.Timeout)
            {
                stopper(TestStatus.TIMEOUT);
                _dumpMessage("***FATAL:Timeout reached!");
            }
        }

        public string GetStoreId()
        {
            return this._fetchStoreID;
        }

        public int GetTotalPreparedOrders()
        {
            return this._counter.TotalPrepared;
        }

        public int GetTotalSentOrders()
        {
            return this._counter.TotalSent;
        }

        public int GetTotalQueuedOrders()
        {
            return this._counter.TotalQueued;
        }

        public int GetTotalBroadcastedOrders()
        {
            return this._counter.TotalBroadcasted;
        }

        public int GetTotalViewedOrders()
        {
            return this._counter.TotalViewed;
        }

        public int GetTotalUnpublishedOrders()
        {
            return this._counter.TotalUnpublished;
        }

        public int GetTotalIgnoredOrders()
        {
            return this._counter.TotalIgnored;
        }

        public int GetTotalFailedOrders()
        {
            return this._counter.TotalPrepared - (this._counter.TotalBroadcasted + this._counter.TotalUnpublished + this._counter.TotalIgnored);
        }

        public int GetCurrentTxId()
        {
            return this._currentTxId;
        }

        public void SavePerformanceCounters(PerformanceCounter counter, string PerformanceDbName)
        {
            try
            {
                var session = this._docStore.OpenSession(PerformanceDbName);
                session.Store(counter);
                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.AddPerformanceCounter()::Failed to save performance counter.\r\n{0}", e.Message));
            }
        }

        public static void InitializeCounters(string DbName, LoadDumpDelegate dump)
        {
            var doc = new DocumentStore { Url = ASConfig.RavenDbUrl, ApiKey = ASConfig.RavenDbApiKey };
            doc.Conventions.MaxNumberOfRequestsPerSession = ASConfig.RavenDbMaxNumReqPerSession;
            doc.Initialize();
            
            // create data for stats
            var counter = new TestCounter
            {
                TotalQueued = 0,
                TotalBroadcasted = 0,
                TotalViewed = 0,
                TotalUnpublished = 0,
                TotalIgnored = 0,
                TotalFailed = 0,
                LastParseQueued = new DateTime(),
                LastParseBroadcasted = new DateTime(),
                LastParseViewed = new DateTime(),
                IsIdle = true
            };

            try
            {
                var session = doc.OpenSession(DbName);
                session.Store(counter);
                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                dump(string.Format("***ERROR: RavenClient.InitializeStats()::Failed to initialize counters.\r\n{0}", e.Message));
            }
        }

        public static void FreeDb(string DbName, LoadDumpDelegate dump)
        {
            var doc = new DocumentStore { Url = ASConfig.RavenDbUrl, ApiKey = ASConfig.RavenDbApiKey };
            doc.Conventions.MaxNumberOfRequestsPerSession = ASConfig.RavenDbMaxNumReqPerSession;
            doc.Initialize();

            try
            {
                var session = doc.OpenSession(DbName);
                var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc);
                stat.IsIdle = true;
                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                dump(string.Format("***ERROR: RavenClient.FreeDb()::Failed to free database.\r\n{0}", e.Message));
            }

            return;
        }

        public static string GetOverallPerformanceReport(string PerformanceDbName, LoadDumpDelegate dump)
        {
            var doc = new DocumentStore { Url = ASConfig.RavenDbUrl, ApiKey = ASConfig.RavenDbApiKey };
            doc.Conventions.MaxNumberOfRequestsPerSession = ASConfig.RavenDbMaxNumReqPerSession;
            doc.Initialize();

            try
            {
                var session = doc.OpenSession(PerformanceDbName);
                var results = session.Query<PerformanceCounter>().ToList<PerformanceCounter>();
                var t = new PerformanceCounter();

                foreach (PerformanceCounter p in results)
                    t.Add(p);
                session.Dispose();
                return t.GetOverallReport();
            }
            catch (Exception e)
            {
                dump(string.Format("***ERROR: RavenClient.GetOverallPerformanceReport()::Failed to get overall performance report from {0}.\r\n{1}", PerformanceDbName, e.Message));
            }

            return null;
        }

        // NOTE: will modify test data information!
        public void PrepareClusteredDataRequest(string StoreId, int TxId, int ClusterSize, bool IsExpIgnore = false)
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var results = session.Query<Order>().Take<Order>(ClusterSize).ToList<Order>();

                for (int i = 0; i < ClusterSize; i++)
                {
                    //results[i].FirstName = string.Format("Tester {0}", i);
                    //results[i].Item = string.Format("Deactivated account item {0}", i);
                    //results[i].Currency = "$";
                    //results[i].Price = string.Format("{0}", i + 12.17);
                    //results[i].Quantity = i + 170;
                    //results[i].FormMessages();

                    results[i].StoreId = StoreId;
                    results[i].TxId = TxId.ToString();
                    results[i].Reset();
                    results[i].IsExpected = true;
                    results[i].IsExpIgnored = IsExpIgnore;

                    this._counter.TotalPrepared += 1;
                }

                session.SaveChanges();
                session.Dispose();

                this._updateTimeoutChecker();
                _dumpMessage("***DEBUG: RavenClient.PrepareRequest()::Request prepared.");
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.PrepareRequest()::Failed to prepare request.\r\n{0}", e.Message));
            }
        }

        public TestCounter GetDbCounters()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var stat = session.Load<TestCounter>(ASConfig.RavenDbCounterDoc).CopyTo();
                session.Dispose();
                return stat;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.GetStats()::Failed to get counters.\r\n{0}", e.Message));
            }
            return null;
        }

        public void UpdateCounters(TestCounter s)
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                var stat = (from r in session.Query<TestCounter>() select r).ToList<TestCounter>();
                //if (stat[0].TotalQueued < s.TotalQueued)
                //    stat[0].TotalQueued = s.TotalQueued;
                //if (stat[0].TotalBroadcasted < s.TotalBroadcasted)
                //    stat[0].TotalBroadcasted = s.TotalBroadcasted;
                //if (stat[0].TotalViewed < s.TotalViewed)
                //    stat[0].TotalViewed = s.TotalViewed;
                if (stat[0].LastParseQueued < s.LastParseQueued)
                    stat[0].LastParseQueued = s.LastParseQueued;
                if (stat[0].LastParseBroadcasted < s.LastParseBroadcasted)
                    stat[0].LastParseBroadcasted = s.LastParseBroadcasted;
                if (stat[0].LastParseViewed < s.LastParseViewed)
                    stat[0].LastParseViewed = s.LastParseViewed;

                session.SaveChanges();
                session.Dispose();
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.UpdateStats()::Failed to update counters.\r\n{0}", e.Message));
            }
        }

        public TestCounter GetTestCounters()
        {
            this._counter.TotalFailed = this._counter.TotalPrepared - (this._counter.TotalBroadcasted + this._counter.TotalUnpublished + this._counter.TotalIgnored);
            return this._counter.CopyTo();
        }

        public List<Order> FetchUnIgnoredOrders()
        {
            try
            {
                var session = this._docStore.OpenSession(this._dbName);
                session.Advanced.MaxNumberOfRequestsPerSession = 100;
                var results = (from r in session.Query<Order>()
                               where (r.IsExpected &&
                               r.IsSent &&
                               r.IsExpIgnored)
                               select r).ToList<Order>();

                var data = new List<Order>();
                foreach (Order o in results)
                {
                    if (o.IsQueued || o.IsBroadcasted || o.IsViewed || o.IsUnpublished)
                        data.Add(o.CopyTo());
                }
                session.Dispose();
                this._counter.TotalIgnored = results.Count - data.Count();
                return data;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: RavenClient.FetchUnviewedOrders()::Failed to fetch unviewed orders.\r\n{0}", e.Message));
            }
            return null;
        }
    }
}
