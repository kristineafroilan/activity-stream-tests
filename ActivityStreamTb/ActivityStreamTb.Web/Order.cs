﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityStreamTb.Web
{
    public class Order
    {
        public string Id { get; set; }
        public string TxId { get; set;}
        public string StoreId { get; set; }
        public string FirstName;
        public string Item;
        public int Quantity;
        public string Price;
        public string StoreUrl;

        public bool IsExpected;
        public bool IsSent;
        public bool IsQueued;
        public bool IsBroadcasted;
        public bool IsViewed;
        public bool IsUnpublished;
        public bool IsExpIgnored;

        public DateTime SentTimestamp;
        public DateTime QueuedTimestamp;
        public DateTime BroadcastedTimestamp;
        public DateTime ViewedTimestamp;

        public TimeSpan SentQueuedSpan;
        public TimeSpan QueuedBroadcastedSpan;
        public TimeSpan BroadcastedViewedSpan;
        public TimeSpan TotalSpan;

        public string OrderQueuedMessage;
        public string NotificationSentMessage;
        public string WidgetMessage;
        public string NotPublishedMessage;

        public string Currency;
        public int ViewCount;

        public void Reset()
        {

            IsExpected = false;
            IsSent = false;

            IsQueued = false;
            IsBroadcasted = false;
            IsViewed = false;
            IsUnpublished = false;
            IsExpIgnored = false;

            ViewCount = 0;
        }

        public Order CopyTo()
        {
            var obj = new Order();
            //obj.Id = this.Id;
            obj.TxId = this.TxId;
            obj.StoreId = this.StoreId;
            obj.IsExpected = this.IsExpected;
            obj.IsSent = this.IsSent;

            obj.FirstName = this.FirstName;
            obj.Item = this.Item;
            obj.Quantity = this.Quantity;
            obj.Price = this.Price;
            obj.StoreUrl = this.StoreUrl;

            obj.IsQueued = this.IsQueued;
            obj.IsBroadcasted = this.IsBroadcasted;
            obj.IsViewed = this.IsViewed;
            obj.IsUnpublished = this.IsUnpublished;
            obj.IsExpIgnored = this.IsExpIgnored;
            
            obj.SentTimestamp = this.SentTimestamp;
            obj.QueuedTimestamp = this.QueuedTimestamp;
            obj.BroadcastedTimestamp = this.BroadcastedTimestamp;
            obj.ViewedTimestamp = this.ViewedTimestamp;

            obj.OrderQueuedMessage = this.OrderQueuedMessage;
            obj.NotificationSentMessage = this.NotificationSentMessage;
            obj.WidgetMessage = this.WidgetMessage;
            obj.NotPublishedMessage = this.NotPublishedMessage;
            obj.Currency = this.Currency;
            obj.ViewCount = this.ViewCount;
            return obj;
        }

        public void SoftUpdate(Order source)
        {
            //this.Id = source.Id;
            this.StoreId = source.StoreId;
            this.TxId = source.TxId;
            this.IsExpected = source.IsExpected;
            this.IsSent = source.IsSent;

            this.FirstName = source.FirstName;
            this.Item = source.Item;
            this.Quantity = source.Quantity;
            this.Price = source.Price;
            this.StoreUrl = source.StoreUrl;

            this.IsQueued = source.IsQueued;
            this.IsBroadcasted = source.IsBroadcasted;
            this.IsViewed = source.IsViewed;
            this.IsUnpublished = source.IsUnpublished;
            this.IsExpIgnored = source.IsExpIgnored;

            this.SentTimestamp = source.SentTimestamp;
            this.QueuedTimestamp = source.QueuedTimestamp;
            this.BroadcastedTimestamp = source.BroadcastedTimestamp;
            this.ViewedTimestamp = source.ViewedTimestamp;

            this.OrderQueuedMessage = source.OrderQueuedMessage;
            this.NotificationSentMessage = source.NotificationSentMessage;
            this.WidgetMessage = source.WidgetMessage;
            this.NotPublishedMessage = source.NotPublishedMessage;
            this.Currency = source.Currency;
            this.ViewCount = source.ViewCount;
        }

        public void CalculateMetrics()
        {
            SentQueuedSpan = QueuedTimestamp.Subtract(SentTimestamp);
            QueuedBroadcastedSpan = BroadcastedTimestamp.Subtract(QueuedTimestamp);
            BroadcastedViewedSpan = ViewedTimestamp.Subtract(BroadcastedTimestamp);
            TotalSpan = ViewedTimestamp.Subtract(SentTimestamp);
        }

        public string Print()
        {
            string text = string.Format("Store Id       = {0}", this.StoreId);
            text = string.Format("{0} \r\nFirstName      = {1}", text, this.FirstName);
            text = string.Format("{0} \r\nItem           = {1}", text, this.Item);
            text = string.Format("{0} \r\nQuantity       = {1}", text, this.Quantity.ToString());
            text = string.Format("{0} \r\nPrice          = {1}", text, this.Price);
            text = string.Format("{0} \r\nStore Url      = {1}", text, this.StoreUrl);

            text = string.Format("{0} \r\nIsExpected     = {1}", text, this.IsExpected.ToString());
            text = string.Format("{0} \r\nIsSent         = {1}", text, this.IsSent.ToString());
            text = string.Format("{0} \r\nIsQueued       = {1}", text, this.IsQueued.ToString());
            text = string.Format("{0} \r\nIsBroadcasted  = {1}", text, this.IsBroadcasted.ToString());
            text = string.Format("{0} \r\nIsViewed       = {1}", text, this.IsViewed.ToString());
            text = string.Format("{0} \r\nIsUnpublished  = {1}", text, this.IsUnpublished.ToString());
            text = string.Format("{0} \r\nIsExpIgnored   = {1}", text, this.IsExpIgnored.ToString());

            text = string.Format("{0} \r\nSentTimestamp  = {1}", text, this.SentTimestamp.ToString());
            text = string.Format("{0} \r\nQueuedTimestamp = {1}", text, this.QueuedTimestamp.ToString());
            text = string.Format("{0} \r\nBroadcastedTime = {1}", text, this.BroadcastedTimestamp.ToString());
            text = string.Format("{0} \r\nViewedTimestamp = {1}", text, this.ViewedTimestamp.ToString());

            text = string.Format("{0} \r\nOrder Queued Message = {1}", text, this.OrderQueuedMessage);
            text = string.Format("{0} \r\nNotif Sent Message   = {1}", text, this.NotificationSentMessage);
            text = string.Format("{0} \r\nWidget Message       = {1}", text, this.WidgetMessage);
            text = string.Format("{0} \r\nNotPublished Message = {1}", text, this.NotPublishedMessage);
            text = string.Format("{0} \r\nCurrency      = {1}", text, this.Currency);
            text = string.Format("{0} \r\nView Count    = {1} views", text, this.ViewCount.ToString());
            text = string.Format("{0} \r\n", text);
            return text;
        }

        public void FormMessages()
        {
            float pricef = 0;

            try
            {
                pricef = (float)System.Convert.ChangeType(Price, typeof(float));
            }
            catch (Exception e)
            {
                pricef = 0;
                Console.WriteLine(e.Message);
            }

            //pricef.ToString("n2") -- will return thousands comma
            //pricef.ToString("0.00") -- will return 2 decimal places
            OrderQueuedMessage = string.Format("FirstName:{0},Item:{1},Quantity:{2},Currency:null,Price:{3},Url:null",  this.FirstName, this.Item, this.Quantity, pricef.ToString("0.00"));
            NotificationSentMessage = string.Format("FirstName:{0},Item:{1},Quantity:{2},Currency:null,Price:{3},Url:null", this.FirstName, this.Item, this.Quantity, pricef.ToString("0.00"));
            NotPublishedMessage = string.Format("FirstName:{0},Item:{1},Quantity:{2},Currency:null,Price:{3},Url:null", this.FirstName, this.Item, this.Quantity, pricef.ToString("0.00"));
            string fname = (string.IsNullOrWhiteSpace(this.FirstName)? this.FirstName: this.FirstName.Trim());
            string it = (string.IsNullOrWhiteSpace(this.Item) ? this.Item : this.Item.Trim());
            //if (this.Quantity > 1)
                WidgetMessage = string.Format("{0} has just purchased {1} {2} for {3}.", this.FirstName, this.Quantity.ToString(), this.Item, pricef.ToString("0.00"));
            //else
            //    WidgetMessage = string.Format("{0} has just purchased a {1} for {2}.", this.FirstName, this.Item, pricef.ToString("0.00"));

            // replace accented characters with ?
            string[] replace = new string[] 
            {
                "ñ", "à", "á", "â", "ã", "ä", "å", "ç", "è", "é", "ê", "ë", "ì", "í", "î", 
                "ð", "ñ", "ò", "ô", "õ", "ö", "ö", "Ã", "Ã", "¡", "Ã", "¢", "Ã", "£", "Ã", 
                "¤", "Ã", "¥", "Ã", "§", "Ã", "¨", "Ã", "©", "Ã", "ª", "Ã", "«", "Ã", "¬", 
                "Ã", "Ã", "®", "Ã", "°", "Ã", "±", "Ã", "²", "Ã", "´", "Ã", "µ", "Ã", "¶", 
                "Ã", "¶", "­"
            };

            foreach(string c in replace)
            {
                OrderQueuedMessage = OrderQueuedMessage.Replace(c, "?");
                NotificationSentMessage = NotificationSentMessage.Replace(c, "?");
                WidgetMessage = WidgetMessage.Replace(c, "?");
                NotPublishedMessage = NotPublishedMessage.Replace(c, "?");
            }
        }
    }
}
