﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Diagnostics;

namespace ActivityStreamTb.Web
{
    public class ScenarioGenerator
    {
        // settings
        private RavenClient raven;
        private LoadDumpDelegate _dumpMessage;

        public ScenarioGenerator(RavenClient Raven, LoadDumpDelegate DumpMessage)
        {
            this.raven = Raven;
            this._dumpMessage = DumpMessage;
        }

        // start engine
        public bool Start(int NumTxRequest)
        {
            // create sample test data
            //raven.CreateOrder();
            _dumpMessage("***DEBUG: ScenarioGenerator.Start()::Scenario generator started.");

            for (int i = 0; i < NumTxRequest; i++)
            {
                // fetch data from test database
                var td = raven.Fetch();

                string jsonData = FormJsonData(td);

                // web client
                var webclient = new WebClient();

                // configure headers
                webclient.Headers.Add(ASConfig.ASApiHeaderType, ASConfig.ASApiHeaderValue);
                webclient.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.Expect100Continue = false;
                
                // timestamp before sending to api
                foreach(Order order in td)
                    order.SentTimestamp = System.DateTime.UtcNow;

                // send data to api
                try
                {
                    string rawresponse = webclient.UploadString(ASConfig.ASApiUrl, jsonData);
                    // update ravendb documents
                    foreach (Order order in td)
                    {
                        order.IsSent = true;
                        raven.UpdatePreparedOrder(order);
                    }
                }
                catch (Exception e)
                {
                    _dumpMessage(string.Format("***ERROR: API failed to respond! \r\n{0}", e.Message));
                    return false;
                }
            }
            return true;
        }

        private string FormJsonData(List<Order> td)
        {
            try
            {
                string json = string.Format("{{\"StoreId\": \"{0}\",", td[0].StoreId);
                json = string.Format("{0}\"OrderItems\": [", json);
                for (int i = 0; i < td.Count; i++)
                {
                    if (i > 0)
                        json = string.Format("{0},", json);
                    json = string.Format("{0}{{\"FirstName\": \"{1}\",", json, td[i].FirstName);
                    json = string.Format("{0}\"Item\": \"{1}\",", json, td[i].Item);
                    json = string.Format("{0}\"Quantity\": \"{1}\",", json, td[i].Quantity.ToString());
                    json = string.Format("{0}\"Price\": \"{1}\",", json, td[i].Price);
                    json = string.Format("{0}\"Url\": \"{1}\"}}", json, td[i].StoreUrl);
                }
                json = string.Format("{0}]}}", json);
                return json;
            }
            catch (Exception e)
            {
                _dumpMessage(string.Format("***ERROR: Error forming json!\r\n{0}", e.Message));
            }
            return null;
        }
    }
}
