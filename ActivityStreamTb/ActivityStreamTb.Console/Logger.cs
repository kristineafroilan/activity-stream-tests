﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using ActivityStreamTb.Web;

namespace ActivityStreamTb.Console
{
    public class Logger
    {        
        private int _dash = 20;
        private string _trace;
        private string _results;
        public string Logname;
        public string RunDate;

        private string _dumptrace;
        private string _dumpresults;

        private ActivityStreamTb.Web.TestCounter _totalcounter;

        public Logger()
        {
            _trace = "";
            _results = "";
            _dumptrace = "";
            _dumpresults = "";
            _totalcounter = new ActivityStreamTb.Web.TestCounter();
        }

        public void CreateLogFile(string path)
        {
            RunDate = System.DateTime.UtcNow.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")) + "." +
                System.DateTime.UtcNow.Hour.ToString("00") +
                System.DateTime.UtcNow.Minute.ToString("00") +
                System.DateTime.UtcNow.Second.ToString("00");

            Logname = string.Format("{0}\\bin\\Test.", path) + RunDate + ".log";

            //logheader = string.Format("{0} \r\nExecution Date: {1} {2}:{3}:{4}", logheader, System.DateTime.UtcNow.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")), System.DateTime.UtcNow.Hour.ToString(), System.DateTime.UtcNow.Minute.ToString(), System.DateTime.UtcNow.Second.ToString());
            System.IO.File.WriteAllText(Logname, FormatHeader("TEST RUN REPORT"));
        }

        public void DumpTrace(string message)
        {
            _trace = string.Format("{0} \r\n{1}", _trace, message);
            System.Console.WriteLine(message);
        }

        public void DumpResults(string message)
        {
            _results = string.Format("{0} \r\n{1}", _results, message);
            System.Console.WriteLine(message);
        }

        public string AddCounter(ActivityStreamTb.Web.TestCounter counter, string testname)
        {
            if (counter != null)
            {
                this._totalcounter.Add(counter);
                string message = FormatHeader(testname.ToUpper());
                message = string.Format("\r\n{0}\r\n{1}", message, counter.Print());
                return message;
            }
            return null;
        }

        public string AddCounter(List<ActivityStreamTb.Web.TestCounter> list, string testname)
        {
            if (list != null)
            {
                string message = FormatHeader(testname.ToUpper());
                int i = 0;
                foreach (ActivityStreamTb.Web.TestCounter counter in list)
                {
                    this._totalcounter.Add(counter);
                    message = string.Format("{0}\r\nSTORE {1}{2}", message, (i+1).ToString(), counter.Print());
                    i += 1;
                }
                return message;
            }
            return null;
        }

        public void LogHeader(string name)
        {
            //_trace = string.Format("{0} \r\n{1}", _trace, this.FormatHeader(name));
            _results = string.Format("{0} \r\n{1}", _results, this.FormatHeader(name));
        }

        public string FormatHeader(string input)
        {
            string msg = "";
            // top header
            for (int i = 0; i < input.Length + 2 + (2 * _dash); i++)
                msg = string.Format("{0}-", msg);
            msg = string.Format("{0} \r\n", msg);

            // input line
            // dash prefix
            for (int i = 0; i < _dash; i++)
                msg = string.Format("{0}-", msg);
            // input
            msg = string.Format("{0} {1} ", msg, input);
            // dash suffix
            for (int i = 0; i < _dash; i++)
                msg = string.Format("{0}-", msg);

            // bottom header
            msg = string.Format("{0} \r\n", msg);
            for (int i = 0; i < input.Length + 2 + (2 * _dash); i++)
                msg = string.Format("{0}-", msg);
            msg = string.Format("{0} \r\n", msg);

            return msg;

        }

        public string GetOverallStats()
        {
            //string source = _dumpresults + _dumptrace;
            string msg = string.Format(" \r\n{0}", FormatHeader("REGRESSION SUMMARY"));
            msg = string.Format("{0} \r\n{1}", msg, this._totalcounter.Print());
            //string msg  = FormatHeader("OVERALL TEST RUN STATISTICS");
            /*
            int success = source.Select((c, i) => source.Substring(i)).Count(sub => sub.StartsWith("***SUCCESS"));
            int fatal   = source.Select((c, i) => source.Substring(i)).Count(sub => sub.StartsWith("***FATAL"));
            int error   = source.Select((c, i) => source.Substring(i)).Count(sub => sub.StartsWith("***ERROR"));
            int warning = source.Select((c, i) => source.Substring(i)).Count(sub => sub.StartsWith("***WARNING"));

            msg = string.Format("{0} \r\n Total SUCCESS MESSAGES: {1}", msg, success.ToString());
            msg = string.Format("{0} \r\n Total FATAL MESSAGES  : {1}", msg, fatal.ToString());
            msg = string.Format("{0} \r\n Total ERROR MESSAGES  : {1}", msg, error.ToString());
            msg = string.Format("{0} \r\n Total WARNING MESSAGES: {1}", msg, warning.ToString());
            msg = string.Format("{0} \r\n-----------------------------------------------------", msg);
            */
            /*
            msg = string.Format("{0} \r\n Total orders prepared : {1} orders", msg, this._totalcounter.TotalPrepared.ToString());
            msg = string.Format("{0} \r\n Sent orders           : {1} orders", msg, this._totalcounter.TotalSent.ToString());
            msg = string.Format("{0} \r\n Queued orders         : {1} orders", msg, this._totalcounter.TotalQueued.ToString());
            msg = string.Format("{0} \r\n Broadcasted orders    : {1} orders", msg, this._totalcounter.TotalBroadcasted.ToString());
            msg = string.Format("{0} \r\n Viewed orders         : {1} orders", msg, this._totalcounter.TotalViewed.ToString());
            msg = string.Format("{0} \r\n Unpublished orders    : {1} orders", msg, this._totalcounter.TotalUnpublished.ToString());
            msg = string.Format("{0} \r\n Ignored orders        : {1} orders", msg, this._totalcounter.TotalIgnored.ToString());
            msg = string.Format("{0} \r\n Failed orders         : {1} orders\r\n", msg, this._totalcounter.TotalFailed.ToString());
            msg = string.Format("{0} \r\n------------------------------------------------------------ \r\n\r\n", msg);
            */

            return msg;
        }

        public string GetReport()
        {
            string message = string.Format("{0}", _results);

            // include trace only if with error(s)
            if(_trace.Contains("***FATAL") || _trace.Contains("***ERROR") || _trace.Contains("***WARNING"))
                message = string.Format("{0} \r\n{1}", message, _trace);

            _dumptrace += _trace;
            _dumpresults += _results;
            _trace = "";
            _results = "";

            return message;
        }

        public void Log(string message)
        {
            System.Console.WriteLine(message);
            System.IO.File.AppendAllText(Logname, string.Format(" \r\n{0}", message));
        }
    }
}
