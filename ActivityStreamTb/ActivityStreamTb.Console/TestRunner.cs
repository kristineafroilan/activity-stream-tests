﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
//using System.IO;
using System.Net.Mail;
using System.Net;

using ActivityStreamTb.Web;

namespace ActivityStreamTb.Console
{
    public class TestRunner
    {
        public static void Main(string[] args)
        {
            string PATH = "C:\\Users\\developer\\Documents\\Projects\\ActivityStreamTb\\ActivityStreamTb\\ActivityStreamTb.Console";
            //string TESTPAGESPATH = string.Format("{0}\\TestPages", PATH);
            string TESTPAGESPATH = "http://acqov5578:9090";

            KillBrowsers();

            var log = new Logger();
            var trace = new LoadDumpDelegate(log.DumpTrace);
            var results = new LoadDumpDelegate(log.DumpResults);

            bool SimpleDtReqEn = false;
            bool ComplexDtReqEn = false;
            bool MultiStoreEn = false;
            bool AlertSentCounterEn = false;
            bool AlertCapStopperEn = false;
            bool DeactivatedAcctEn = false;
            bool AlertReachedEn = false;
            bool ImportEn = false;
            bool ReportsEn = false;

            int numTx = 20;
            int numStores = 20;
            int StartIdx = 1;
            int EndIdx = 20;            

            // parse arguments
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "Path":
                        PATH = args[i + 1].ToString();
                        //TESTPAGESPATH = string.Format("{0}\\TestPages", PATH);
                        i += 1;
                        break;
                    case "NumTx":
                        numTx = (int)System.Convert.ChangeType(args[i + 1], typeof(int));
                        i += 1;
                        break;
                    case "NumStores":
                        numStores = (int)System.Convert.ChangeType(args[i + 1], typeof(int));
                        i += 1;
                        break;
                    case "Import":
                        ImportEn = true;
                        StartIdx = (int)System.Convert.ChangeType(args[i + 1], typeof(int));
                        EndIdx = (int)System.Convert.ChangeType(args[i + 2], typeof(int));
                        i += 2;
                        break;
                    case "All":
                        SimpleDtReqEn = true;
                        ComplexDtReqEn = true;
                        MultiStoreEn = true;
                        AlertSentCounterEn = true;
                        AlertCapStopperEn = true;
                        DeactivatedAcctEn = true;
                        AlertReachedEn = true;
                        break;
                    case "SimpleDataRequest": SimpleDtReqEn = true; break;
                    case "ComplexDataRequest": ComplexDtReqEn = true; break;
                    case "MultiStore": MultiStoreEn = true; break;
                    case "AlertSentCounter": AlertSentCounterEn = true; break;
                    case "AlertCapStopper": AlertCapStopperEn = true; break;
                    case "CancelledAcct": DeactivatedAcctEn = true; break;
                    case "AlertReached": AlertReachedEn = true; break;
                    case "Reports": ReportsEn = true; break;
                }
            }

            log.CreateLogFile(PATH);
            log.Log(string.Format("Setting execution path to {0}", PATH));
            ShowOptions(log);

            if (args.Length < 1)
            {
                //GetStoreId();
                //CreateTestPages(string.Format("{0}\\TestPages", PATH), log);

                numTx = 5;
                numStores = 5;

                SimpleDtReqEn = true;
                ComplexDtReqEn = true;
                AlertCapStopperEn = true;
                AlertSentCounterEn = true;
                AlertReachedEn = true;
                DeactivatedAcctEn = true;
                MultiStoreEn = true;
                //ReportsEn = true;
            }

            KillBrowsers();

            string ResultPerTest = "";
            if (SimpleDtReqEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, SingleStore(log, numTx, trace, results, TESTPAGESPATH));
            if (ComplexDtReqEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, SingleStore(log, numTx, trace, results, TESTPAGESPATH, false));
            if (AlertCapStopperEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, AlertCapStopper(log, numTx, trace, results, TESTPAGESPATH));
            if (MultiStoreEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, MultiStore(log, numTx, numStores, trace, results, TESTPAGESPATH));
            if (AlertSentCounterEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, AlertSentCounter(log, trace, results));
            if (AlertReachedEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, AlertReached(log, numTx, trace, results, TESTPAGESPATH));
            if (DeactivatedAcctEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, CancelledSubscription(log, numTx, trace, results, TESTPAGESPATH));
            if (ImportEn)
                Import(log, StartIdx, EndIdx, results, PATH);
            if(ReportsEn)
                ResultPerTest = string.Format("{0}\r\n{1}", ResultPerTest, AdminReports(log, trace, results));

            System.Threading.Thread.Sleep(1000);
            log.Log(log.GetOverallStats());
            log.Log(RavenClient.GetOverallPerformanceReport(ASConfig.RavenDbPerformanceDbName, results));
            log.Log("TESTS EXECUTED:");
            log.Log(ResultPerTest);

            //EmailResults(log);

            log.Log("Activity Stream Test Runner Done.");
            return;
        }

        static void KillBrowsers()
        {
            try
            {
                foreach (Process c in Process.GetProcessesByName("chrome"))
                    c.Kill();
                //foreach (Process f in Process.GetProcessesByName("firefox"))
                //    f.Kill();
            }

            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

            return;
        }

        static Process StartUrl(string url)
        {
            Process process = Process.Start(url);
            System.Threading.Thread.Sleep(1000);
            return process;
        }

        static void EmailResults(Logger log)
        {
            var mail = new MailMessage();

            mail.From = new MailAddress("kristine@oxygenventures.com.au", "Daily Test Runs");
            string password = "HANN1bal123";

            mail.To.Add(new MailAddress("kristine@oxygenventures.com.au", "To Kristine Afroilan"));
            mail.To.Add(new MailAddress("qa@oxygenventures.com.au", "QA"));
            mail.To.Add(new MailAddress("markb@oxygenventures.com.au", "Mark Bandillo"));

            mail.Subject = string.Format("Daily Runs - {0}", log.RunDate);
            mail.Body = log.GetOverallStats();
            mail.Attachments.Add(new System.Net.Mail.Attachment(log.Logname));

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            //smtp.Port = 465;
            //smtp.Port = 25;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new System.Net.NetworkCredential(mail.From.Address, password);
            smtp.Timeout = 20000;


            try
            {
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                log.Log(string.Format("Error sending email.\r\n{0}", e.Message));
            }
        }

        static void ShowOptions(Logger log)
        {
            log.Log(string.Format("Starting Activity Stream Test Runner {0}", System.DateTime.UtcNow.ToString()));
            log.Log("Test Runner Help");
            log.Log("Path <path>              - Set-up test execution path");
            log.Log("NumTx <num>              - Set-up number of transaction requests");
            log.Log("NumStores <num>          - Set-up number of stores for multi-store test");
            log.Log("Import <startId> <endId> - Import test data for multi-store test");
            log.Log("All                      - Execute all tests");
            log.Log("SimpleDataRequest        - Test for single store simple data request");
            log.Log("ComplexDataRequest       - Test for single store complex data request ");
            log.Log("MultiStore               - Test for multiple stores");
            log.Log("AlertSentCounter         - Test for alert sent counter");
            log.Log("AlertCapStopper          - Test for alert cap stopper");
            log.Log("CancelledAcct            - Test for cancelled accounts");
            log.Log("AlertReached             - Test for alert reached accounts");
        }

        static string SingleStore(Logger log, int numTx, LoadDumpDelegate trace, LoadDumpDelegate results, string TESTPAGESPATH, bool IsSimple = true)
        {
            string testname = "";
            string storeid = "";
            string dbname = "";
            Process p;
            if (IsSimple)
            {
                testname = "Single Store Simple Data Request Test";
                RavenClient.FreeDb(ASConfig.TestSingleStoreDb, results);
                p = StartUrl(string.Format("{0}/TestSingleStore.html", TESTPAGESPATH));
                storeid = ASConfig.TestSingleStoreId;
                dbname = ASConfig.TestSingleStoreDb;
            }
            else
            {
                testname = "Single Store Complex Data Request Test";
                RavenClient.FreeDb(ASConfig.TestSingleStoreComplexDb, results);
                p = StartUrl(string.Format("{0}/TestSingleStoreComplex.html", TESTPAGESPATH));
                storeid = ASConfig.TestSingleStoreComplexId;
                dbname = ASConfig.TestSingleStoreComplexDb;
            }
            
            log.Log(string.Format("Executing {0} with {1} transactions... ", testname, numTx));
            log.LogHeader(testname.ToUpper());
            var simple = new TestSingleStore(trace, results, numTx, storeid, dbname);
            var testresult = log.AddCounter(simple.Start(IsSimple), testname);
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());
            KillProcess(p);

            return testresult;
        }

        static string MultiStore(Logger log, int numTx, int numStores, LoadDumpDelegate trace, LoadDumpDelegate results, string TESTPAGESPATH)
        {
            List<Process> ProcessAry = new List<Process>();
            string testname = "Multiple Store Data Request Test";
            
            for (int j = 0; j < numStores; j++)
            {
                RavenClient.FreeDb(string.Format("TestMultiStore{0}", (j + 1).ToString()), results);
                string url = string.Format("{0}/TestMultiStore{1}.html", TESTPAGESPATH, j + 1);
                ProcessAry.Add(StartUrl(url));
            }
            System.Threading.Thread.Sleep(4000);
            log.Log(string.Format("Executing {0} with {1} transactions {2} store(s)...", testname, numTx, numStores));
            log.LogHeader(testname.ToUpper());
            var multi = new TestMultiStore(trace, results, numTx, numStores);
            var testresult = log.AddCounter(multi.Start(), testname);
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());
            KillProcess(ProcessAry);

            return testresult;
        }

        static string AlertSentCounter(Logger log, LoadDumpDelegate trace, LoadDumpDelegate results)
        {
            string testname = "Alert Sent Counter Test";

            log.Log(string.Format("Executing {0}...", testname));
            log.LogHeader(testname.ToUpper());
            var alertsentcounter = new TestAlertSentCounter(trace, results);
            var result = alertsentcounter.Start();
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());

            return string.Format("{0}\r\n{1}", log.FormatHeader(testname.ToUpper()), result);
        }

        static string AlertCapStopper(Logger log, int numTx, LoadDumpDelegate trace, LoadDumpDelegate results, string TESTPAGESPATH)
        {
            string testname = "Alert Cap Stopper Test";

            RavenClient.FreeDb(ASConfig.TestAlertCapStopperDb, results);
            Process p = StartUrl(string.Format("{0}/TestAlertCapStopper.html", TESTPAGESPATH));
            log.Log(string.Format("Executing {0} with {1} transactions...", testname, numTx));
            log.LogHeader(testname.ToUpper());
            var alertcapstopper = new TestAlertCapStopper(trace, results, numTx);
            var testresult = log.AddCounter(alertcapstopper.Start(), testname);
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());
            KillProcess(p);

            return testresult;
        }

        static string AlertReached(Logger log, int numTx, LoadDumpDelegate trace, LoadDumpDelegate results, string TESTPAGESPATH)
        {
            string testname = "Alert Reached Test";

            RavenClient.FreeDb(ASConfig.TestAlertReachedDb, results);
            log.Log(string.Format("Executing {0} with {1} transactions...", testname, numTx));
            log.LogHeader(testname.ToUpper());
            var alertreached = new TestAlertReached(trace, results, numTx);
            var testresult = log.AddCounter(alertreached.Start(), testname);
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());

            return testresult;
        }

        static string CancelledSubscription(Logger log, int numTx, LoadDumpDelegate trace, LoadDumpDelegate results, string TESTPAGESPATH)
        {
            string testname = "Cancelled Accounts Test";

            RavenClient.FreeDb(ASConfig.TestDeactivatedAccountDb, results);
            log.Log(string.Format("Executing {0} with {1} transactions...", testname, numTx));
            log.LogHeader(testname.ToUpper());
            var deactivated = new TestDeactivatedAccount(trace, results, numTx);
            var testresult = log.AddCounter(deactivated.Start(), testname);
            log.Log("Deactivated Subscription Test Done");
            log.Log(log.GetReport());

            return testresult;
        }

        static void Import(Logger log, int startId, int endId, LoadDumpDelegate results, string PATH)
        {
            for (int i = startId; i <= endId; i++)
            {
                string db = string.Format("TestMultiStore{0}", i);
                try
                {
                    var test = new ImportTestData();
                    log.Log(string.Format("Importing test data to database {0}.", db));
                    test.ImportFile(db, string.Format("{0}\\TestData\\FirstName.tsv", PATH), results);
                }
                catch (Exception e)
                {
                    log.Log(string.Format("Error importing test data to database {0}. \r\n{1}", db, e.Message));
                }
            }
        }

        static void CreateTestCounter(Logger log, int startId, int endId, LoadDumpDelegate results, string PATH)
        {
            for (int i = startId; i <= endId; i++)
            {
                string db = string.Format("TestMultiStore{0}", i);
                try
                {
                    log.Log(string.Format("Initializing test counters for database {0}.", db));
                    RavenClient.InitializeCounters(db, results);
                }
                catch (Exception e)
                {
                    log.Log(string.Format("Initializing test counters for database {0}. \r\n{1}", db, e.Message));
                }
            }
        }

        static void KillProcess(List<Process> ProcessAry)
        {
            foreach (Process p in ProcessAry)
                KillProcess(p);
            ProcessAry.Clear();
        }

        static void KillProcess(Process p)
        {
            try
            {
                if (!p.HasExited)
                {
                    p.Kill();
                    System.Threading.Thread.Sleep(500);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        static void GetStoreId()
        {
            System.IO.File.WriteAllText("test.txt", "Test");
            var stores = AmazonSqlClient.GetStoreId();
            foreach (Merchant store in stores)
            {
                var num = store.Email.Replace("kristine+tbms", "").Replace("@oxygenventures.com.au", "");
                string str = string.Format("\r\npublic static string TestMultiStoreId{0} = \"{1}\";", num, store.StoreId);
                System.IO.File.AppendAllText("test.txt", str);
            }
        }

        static void CreateTestPages(string TESTPAGESPATH, Logger log)
        {
            string content;

            var stores = AmazonSqlClient.GetStoreId();
            foreach(Merchant store in stores)
            {
                try
                {
                    var num = (int) System.Convert.ChangeType(store.Email.Replace("kristine+tbms", "").Replace("@oxygenventures.com.au", ""), typeof(int));

                    string f = string.Format("{0}\\TestMultiStore1.html", TESTPAGESPATH);

                    using (var reader = new System.IO.StreamReader(f))
                    {
                        content = reader.ReadToEnd();
                    }

                    content = content.Replace(ASConfig.TestMultiStoreId1, store.StoreId);
                    System.IO.File.WriteAllText(string.Format("{0}\\dump\\TestMultiStore{1}.html", TESTPAGESPATH, num.ToString()), content);
                    if(store.StoreId != ASConfig.GetMultiStoreId(num))
                        log.Log(string.Format("***ERROR: Store ID mismatch {0}: html {1} : asconfig {2}", num.ToString(), store.StoreId, ASConfig.GetMultiStoreId(num)));
                }

                catch (Exception e)
                {
                    log.Log(string.Format("***ERROR: Execution Error.\r\n{0}", e.Message));
                }
            }
        }

        static string AdminReports(Logger log, LoadDumpDelegate trace, LoadDumpDelegate results)
        {
            string testname = "Admin Reports Test";

            log.Log(string.Format("Executing {0}...", testname));
            log.LogHeader(testname.ToUpper());
            var test = new TestAdminReportAlertsPerCustomer(trace, results);
            var result = test.Start();
            log.Log(string.Format("{0} Done", testname));
            log.Log(log.GetReport());

            return string.Format("{0}\r\n{1}", log.FormatHeader(testname.ToUpper()), result);
        }
    }
}
