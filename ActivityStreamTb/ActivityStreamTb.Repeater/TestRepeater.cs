﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
//using System.IO;
using System.Net.Mail;
using System.Net;

using ActivityStreamTb.Web;
using ActivityStreamTb.Console;

namespace ActivityStreamTb.Repeater
{
    class TestRepeater
    {
        static void Main(string[] args)
        {
            string PATH = "C:\\Users\\developer\\Documents\\Projects\\ActivityStreamTb\\ActivityStreamTb\\ActivityStreamTb.Console";
            string TESTPAGESPATH = string.Format("{0}\\TestPages", PATH);
            int numTx = 5;
            var log = new ActivityStreamTb.Console.Logger();
            var trace = new LoadDumpDelegate(log.DumpTrace);
            var results = new LoadDumpDelegate(log.DumpResults);

            log.CreateLogFile(PATH);
                
            KillBrowsers();

            int i = 0;
            while (true)
            {
                string testname = "Single Store Simple Data Request Test";
                Process process = StartUrl(string.Format("{0}\\Test Single Store.html", TESTPAGESPATH));
                log.Log(string.Format("Executing {0} with {1} transactions... ", testname, numTx));
                log.LogHeader(testname.ToUpper());
                var simple = new TestSingleStore(trace, results, numTx);
                var testresult = log.AddCounter(simple.Start(true), testname);
                log.Log(string.Format("{0} Done", testname));
                log.Log(log.GetReport());

                if (!process.HasExited)
                {
                    //process.CloseMainWindow();
                    process.Kill();
                }

                // execute for ~6 hours
                if (i > 72)
                {
                    log.Log("Execution complete!");
                    break;
                }
                else
                {
                    log.Log("Suspending execution for 4 minutes");
                    // wait for 5 minutes
                    System.Threading.Thread.Sleep(300000);
                    i += 1;
                }
            }

            System.Threading.Thread.Sleep(1000);
            log.Log(log.GetOverallStats());
            log.Log("Activity Stream Test Runner Done.");
            return;
        }

        static void KillBrowsers()
        {
            try
            {
                foreach (Process c in Process.GetProcessesByName("chrome"))
                    c.Kill();
                //foreach (Process f in Process.GetProcessesByName("firefox"))
                //    f.Kill();
            }

            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

            return;
        }

        static Process StartUrl(string url)
        {
            Process process = Process.Start(url);
            System.Threading.Thread.Sleep(4000);
            return process;
        }
    }
}
