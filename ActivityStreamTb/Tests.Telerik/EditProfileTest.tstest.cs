using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class EditProfileTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        public Merchant store, editedstore;
        public string email;
        
        // Add your test methods here...
    
        [CodedStep(@"Fetch store details")]
        public void FetchStoreDetails_CodedStep()
        {
            this.email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            store = AmazonSqlClient.FetchStore(this.email);
            Log.WriteLine(store.Print());
        }
    
        [CodedStep(@"Verify Initial Load of Profile")]
        public void VerifyInitial_CodedStep()
        {
            CompareProfile(store);
        }
    
        [CodedStep(@"Cancel Changes")]
        public void CancelChanges_CodedStep()
        {
            EditSteps("CancelChanges");
        }
        
        [CodedStep(@"Verify Profile after Cancelled Changes")]
        public void VerifyCancelledChanges_CodedStep()
        {
            CompareProfile(store);
            var s = AmazonSqlClient.FetchStore(this.email);
            Assert.IsTrue(store.CompareProfile(s), string.Format("***ERROR: Store details changed in database after cancelled changes!\r\n[Exp:]\r\n{0}\r\n[Act:]\r\n{1}", store.Print(), s.Print()));
        }
        
        [CodedStep(@"Cancel Saving of Changes")]
        public void CancelSaving_CodedStep()
        {
            EditSteps("CancelSaving");
        }
        
        [CodedStep(@"Verify Cancelled Save")]
        public void VerifyCancelledSave_CodedStep()
        {
            CompareProfile(store);
            var s = AmazonSqlClient.FetchStore(this.email);
            Assert.IsTrue(store.CompareProfile(s), string.Format("***ERROR: Store details changed in database after cancellation of save changes!\r\n[Exp:]\r\n{0}\r\n[Act:]\r\n{1}", store.Print(), s.Print()));
        }
        
        [CodedStep(@"Edit Success")]
        public void EditSuccess_CodedStep()
        {
            EditSteps("Edited");            
        }
        
        [CodedStep(@"Verify Edited Profile")]
        public void VerifyEditedProfile_CodedStep()
        {
            CompareProfile(editedstore);
            var s = AmazonSqlClient.FetchStore(editedstore.Email);
            Assert.IsTrue(editedstore.CompareProfile(s), string.Format("***ERROR: Store profile in database not equal to edited profile!\r\n[Exp:]\r\n{0}\r\n[Act:]\r\n{1}", editedstore.Print(), s.Print()));            
        }
        
        [CodedStep(@"Revert Edit")]
        public void RevertEdit_CodedStep()
        {            
            editedstore = store.CopyTo();
            editedstore.Address1 += "Edited";
            editedstore.Address2 += "Edited";
            editedstore.City += "Edited";
            editedstore.State += "Edited";
            editedstore.ZipCode += "Edited";
            editedstore.Country = Randomizer.GetRandomCountry();
            Log.WriteLine(string.Format("REVERT CHANGES\r\n{0}", editedstore.Print()));
            EditProfile(editedstore);
        }
        
        [CodedStep(@"Verify Reverted Edit")]
        public void VerifyRevertedEdit_CodedStep()
        {
            CompareProfile(editedstore);
            var s = AmazonSqlClient.FetchStore(editedstore.Email);
            Assert.IsTrue(editedstore.CompareProfile(s), string.Format("***ERROR: Store profile in database not reverted to original!\r\n[Exp:]\r\n{0}\r\n[Act:]\r\n{1}", editedstore.Print(), s.Print()));
        }

        private void EditSteps(string appendsuffix)
        {
            editedstore = store.CopyTo();
            editedstore.AppendStringToProfile(appendsuffix);
            editedstore.Country = Randomizer.GetRandomCountry();
            Log.WriteLine(string.Format("{0}\r\n{1}", appendsuffix.ToUpper(), editedstore.Print()));
            EditProfile(editedstore);
        }
        
        private void CompareProfile(Merchant store)
        {
            ActiveBrowser.RefreshDomTree();
            Pages.ActivityStreamProfile.ProfileNameDiv.Wait.ForVisible(30000);
            Pages.ActivityStreamProfile.ProfileNameDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, string.Format("{0} {1}", store.FirstName, store.LastName));
            Pages.ActivityStreamProfile.ProfileEmailDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, store.Email);
            Pages.ActivityStreamProfile.ProfilePhoneDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, store.ContactNumber);
            Pages.ActivityStreamProfile.ProfileCountryDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, store.Country);
            //Pages.ActivityStreamProfile.ProfileAddressDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, string.Format("{0}, {1}, {2}, {3}", store.Address1, store.Address2, store.City, store.State));
            Pages.ActivityStreamProfile.ProfileZipcodeDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, store.ZipCode);
            Pages.ActivityStreamProfile.ProfileUrlDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, store.StoreUrl);
        }
    
        private void EditProfile(Merchant s)
        {
            Pages.ActivityStreamProfile.FirstNameText.Wait.ForVisible(30000);
            Actions.SetText(Pages.ActivityStreamProfile.FirstNameText, s.FirstName);
            Actions.SetText(Pages.ActivityStreamProfile.LastNameText, s.LastName);
            Actions.SetText(Pages.ActivityStreamProfile.ContactNumberText, s.ContactNumber);
            Actions.SetText(Pages.ActivityStreamProfile.UrlText, s.StoreUrl);
            Actions.SetText(Pages.ActivityStreamProfile.EmailText, s.Email);
            Actions.SetText(Pages.ActivityStreamProfile.AddressLine1Text, s.Address1);
            Actions.SetText(Pages.ActivityStreamProfile.AddressLine2Text, s.Address2);
            Actions.SetText(Pages.ActivityStreamProfile.CityText, s.City);
            Actions.SetText(Pages.ActivityStreamProfile.StateText, s.State);
            Actions.SetText(Pages.ActivityStreamProfile.ZipCodeText, s.ZipCode);
            
            Log.WriteLine(string.Format("Selecting country {0}", s.Country));
            //Pages.ActivityStreamProfile.CountrySelectDiv.ScrollToVisible();
            //Pages.ActivityStreamProfile.CountrySelectDiv.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 149, -17, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            Pages.ActivityStreamProfile.CountrySelect.ScrollToVisible();
            Pages.ActivityStreamProfile.CountrySelect.SelectByValue(s.Country);
        }
    }
}
