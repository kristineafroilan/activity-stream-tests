using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class AppearancePortalvsDBTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        Merchant app;
        
        // Add your test methods here...
    
        [CodedStep(@"Verify input 'BackgroundColorText' value 'Exact' '00ffc3'.")]
        public void Appearance__Default_CodedStep()
        {
            // Verify input 'BackgroundColorText' value 'Exact' '00ffc3'.
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            app = AmazonSqlClient.FetchStoreAppearance(email);                        
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BackgroundColorText.Text, app.BackgroundColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BackgroundColor, Pages.ActivityStreamAppearance.BackgroundColorText.Text));
            
        }
    
        [CodedStep(@"Verify input 'BorderColorText' value 'Exact' '000000'.")]
        public void Appearance__Default_CodedStep1()
        {
            // Verify input 'BorderColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BorderColorText.Value, app.BorderColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BorderColor, Pages.ActivityStreamAppearance.BorderColorText.Text));
            
        }
    
        [CodedStep(@"Verify input 'FontColorText' value 'Exact' '000000'.")]
        public void Appearance__Default_CodedStep2()
        {
            // Verify input 'FontColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.FontColorText.Value, app.FontColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.FontColor, Pages.ActivityStreamAppearance.FontColorText.Text));
            
        }
    
    
    
    
        //[CodedStep(@"Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'")]
        //public void Appearance__Default_CodedStep3()
        //{
            //// Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'
            //if(Pages.ActivityStreamAppearance.AlertWillAsideTag.BorderColor == "#0CB09C")
            //{position = "topLeft";}
            
        //}
    
        [CodedStep(@"Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'")]
        public void Appearance__Default_CodedStep3()
        {
            // Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'
            string position;
            
            string topLeftColor = "#E5E5E5";
            string topRightColor = "#E5E5E5";
            string bottomLeftColor = "#E5E5E5";
            string bottomRightColor = "#E5E5E5";
            
            switch(app.AlertPosition)
            {
                case "topLeft": topLeftColor = "#0CB09C"; break;
                case "topRight": topRightColor = "#0CB09C"; break;
                case "bottomLeft": bottomLeftColor = "#0CB09C"; break;
                case "bottomRight": bottomRightColor = "#0CB09C"; break;
            }
            
            Pages.ActivityStreamAppearance.AlertWillAsideTag.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag0.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag1.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag2.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            
        }
    }

        
    }
