using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;
using Newtonsoft.Json;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class DashboardValidationTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
        
        public Merchant store = new Merchant();
    
        [CodedStep(@"Verify 'TextContent' 'on 'MyPlanSpan'")]
        public void VerifyMyPlan_CodedStep()
        {
            // Verify 'TextContent' 'Contains' 'My Plan :' on 'MyPlanSpan'            
            Pages.ActivityStreamDashboard.MyPlanSpan.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, string.Format("My Plan :{0}", store.PlanName));            
        }
    
        [CodedStep(@"Verify 'TextContent' on 'AlertUsageTag'")]
        public void VerifyAlertUsage_CodedStep()
        {
            // Verify 'TextContent' 'Contains' 'alerts usedthis Month' on 'AlertUsageTag'
            Pages.ActivityStreamDashboard.AlertUsageTag.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, string.Format("{0}alerts usedthis Month", store.AlertUsage.ToString()));
        }
    
        [CodedStep(@"Verify 'TextContent' on 'MonthlyAlertQuotaTag'")]
        public void VerifyMonthlyAlertQuota_CodedStep()
        {
            // Verify 'TextContent' 'Contains' '(1,000 monthly alert quota)Quota Period:' on 'MonthlyAlertQuotaTag'
            Pages.ActivityStreamDashboard.MonthlyAlertQuotaTag.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, string.Format("({0} monthly alert quota)Quota Period:", store.TotalNotificationAllowed.ToString("N0")));
        }
    
        [CodedStep(@"Verify 'TextContent'on 'QuotaPeriodSpan'")]
        public void VerifyQuotaPeriod_CodedStep()
        {
            // Verify 'TextContent' 'Contains' 'Sep 16, 2013 to Oct 15, 2013' on 'QuotaPeriodSpan'
            DateTime end = store.StartDate.AddMonths(1).AddDays(-1);
            string period = string.Format("{0:MMM dd, yyyy} to {1:MMM dd, yyyy}", store.StartDate, end);
            Pages.ActivityStreamDashboard.QuotaPeriodSpan.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Contains, period);            
        }
    
        [CodedStep(@"Fetch Store Details")]
        public void FetchStoreDetails_CodedStep()
        {
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            store = AmazonSqlClient.FetchStore(email);
            store.AlertUsage = CountAlertSent(store.Username);
            Log.WriteLine(store.Print());
        }
        
        private int CountAlertSent(string storeid)
        {
            try
            {
                var uri = string.Format("http://testapi.activitystream.io:2113/projection/$TbNotificationSentEvents/state?partition={0}", storeid);
                var json = DownloadJson(uri, storeid);
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<NotificationReceivedEventItems>(json);

                if (list != null)
                {
                    return list.Items.Count;
                }
            }
            catch (Exception e)
            {
                Assert.IsTrue(false, string.Format("***ERROR: Listener.CountAlertSent()::Exception encountered for Store {0}\r\n{1}", storeid, e.Message));
            }

            return 0;
        }
        
        private string DownloadJson(string uri, string storeid)
        {
            System.Net.WebClient webclient = new System.Net.WebClient();
            webclient.Credentials = new System.Net.NetworkCredential("activitystream", "changeit");
            webclient.Headers.Add("Content-Type", "application/json");

            string json;
            while (true)
            {
                try
                {
                    json = webclient.DownloadString(uri);
                    break;
                }
                catch (Exception e)
                {
                    Assert.IsTrue(false, string.Format("***ERROR: Unable to download data from \r\n{0}!\r\n{1}", uri, e.Message));
                }
            }

            if(!json.Equals(""))
                json = json.Replace(storeid, "Items");
            return json;
        }
    }
    
    public class NotificationReceivedEventItems
    {
        public List<NotificationReceivedEvent> Items { get; set; }
    }

    public class NotificationReceivedEvent
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string UserCookie { get; set; }
    }
}
