using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class AppearanceEditResetCancelSaveTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        Merchant app;
       Merchant n;
        
        // Add your test methods here...
    
        [CodedStep(@"Color input values")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep()
        {
            // Verify input 'BackgroundColorText' value 'Exact' 'ffffff'.
            var random = new Random();
            var color = String.Format("{0:X6}", random.Next(0x1000000));
            var color2 = String.Format("{0:X6}", random.Next(0x1000000));
            var color3 = String.Format("{0:X6}", random.Next(0x1000000));
            Pages.ActivityStreamAppearance.BackgroundColorText.Value = color;
            Pages.ActivityStreamAppearance.BorderColorText.Value = color2;
            Pages.ActivityStreamAppearance.FontColorText.Value = color3;
            n = new Merchant();
            n.BackgroundColor = color;
            n.BorderColor = color2;
            n.FontColor = color3;
        }
    
        
    
        [CodedStep(@"Verify BackgroundColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep1()
        {
            // Verify input 'BackgroundColorText' value 'Exact' '00ffc3'.
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            app = AmazonSqlClient.FetchStoreAppearance(email);                        
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BackgroundColorText.Text, app.BackgroundColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BackgroundColor, Pages.ActivityStreamAppearance.BackgroundColorText.Text));
            
        }
    
        [CodedStep(@"Verify input BorderColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep2()
        {
            // Verify input 'BorderColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BorderColorText.Value, app.BorderColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BorderColor, Pages.ActivityStreamAppearance.BorderColorText.Text));
            
        }
    
        [CodedStep(@"Verify input FontColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep3()
        {
            // Verify input 'FontColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.FontColorText.Value, app.FontColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.FontColor, Pages.ActivityStreamAppearance.FontColorText.Text));
            
        }
    
        [CodedStep(@"Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep4()
        {
            // Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'
          //  string position;
            
            string topLeftColor = "#E5E5E5";
            string topRightColor = "#E5E5E5";
            string bottomLeftColor = "#E5E5E5";
            string bottomRightColor = "#E5E5E5";
            
            switch(app.AlertPosition)
            {
                case "topLeft": topLeftColor = "#0CB09C"; break;
                case "topRight": topRightColor = "#0CB09C"; break;
                case "bottomLeft": bottomLeftColor = "#0CB09C"; break;
                case "bottomRight": bottomRightColor = "#0CB09C"; break;
            }
            
            Pages.ActivityStreamAppearance.AlertWillAsideTag.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag0.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag1.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag2.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            
        }
    
        [CodedStep(@"Desktop command: LeftClick on AlertWillAsideTag")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep5()
        {
            // Desktop command: LeftClick on AlertWillAsideTag
            int num = Randomizer.GetRandomNumber(1, 4);
          //  string one;
          //  string two;
          //  string three;
          //  string four;
                                   
            
            switch(num)
            {
                case 1: 
                    Pages.ActivityStreamAppearance.AlertWillAsideTag.Wait.ForExists(30000);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementTopAtWindowTop);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
                   // one = "topLeft"; 
                    break;
                case 2: 
                    Pages.ActivityStreamAppearance.AlertWillAsideTag0.Wait.ForExists(30000);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag0.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementTopAtWindowTop);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag0.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
                   // two = "topRight"; 
                    break;
                case 3: 
                    Pages.ActivityStreamAppearance.AlertWillAsideTag1.Wait.ForExists(30000);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag1.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementTopAtWindowTop);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag1.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
                   // three = "bottomLeft"; 
                    break;
                case 4: 
                    Pages.ActivityStreamAppearance.AlertWillAsideTag2.Wait.ForExists(30000);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag2.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementTopAtWindowTop);
                    Pages.ActivityStreamAppearance.AlertWillAsideTag2.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
                  //  four = "bottomRight"; 
                    break;
            }
            
        }
    
        //[CodedStep(@"Click 'BtnResetButton'")]
        //public void Appearance__Edit_Cancel_and_Save_CodedStep6()
        //{
            //// Click 'BtnResetButton'
            //ActiveBrowser.Window.SetFocus();
            //Pages.ActivityStreamAppearance.BtnResetButton.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementTopAtWindowTop);
            //Pages.ActivityStreamAppearance.BtnResetButton.MouseClick();
            
        //}
    
        
    
        [CodedStep(@"Verify BackgroundColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep7()
        {
            // Verify input 'BackgroundColorText' value 'Exact' '00ffc3'.
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            app = AmazonSqlClient.FetchStoreAppearance(email);                        
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BackgroundColorText.Text, app.BackgroundColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BackgroundColor, Pages.ActivityStreamAppearance.BackgroundColorText.Text));
            
        }
    
        [CodedStep(@"Verify input BorderColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep8()
        {
            // Verify input 'BorderColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BorderColorText.Value, app.BorderColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BorderColor, Pages.ActivityStreamAppearance.BorderColorText.Text));
            
        }
    
        [CodedStep(@"Verify input FontColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep9()
        {
            // Verify input 'FontColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.FontColorText.Value, app.FontColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.FontColor, Pages.ActivityStreamAppearance.FontColorText.Text));
            
        }
    
        [CodedStep(@"Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep10()
        {
            // Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'
            //string position;
            
            string topLeftColor = "#E5E5E5";
            string topRightColor = "#E5E5E5";
            string bottomLeftColor = "#E5E5E5";
            string bottomRightColor = "#E5E5E5";
            
            switch(app.AlertPosition)
            {
                case "topLeft": topLeftColor = "#0CB09C"; break;
                case "topRight": topRightColor = "#0CB09C"; break;
                case "bottomLeft": bottomLeftColor = "#0CB09C"; break;
                case "bottomRight": bottomRightColor = "#0CB09C"; break;
            }
            
            Pages.ActivityStreamAppearance.AlertWillAsideTag.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag0.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag1.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag2.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            
        }
    
        [CodedStep(@"Verify BackgroundColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep11()
        {
            // Verify input 'BackgroundColorText' value 'Exact' '00ffc3'.
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            app = AmazonSqlClient.FetchStoreAppearance(email);                        
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BackgroundColorText.Text, app.BackgroundColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BackgroundColor, Pages.ActivityStreamAppearance.BackgroundColorText.Text));
            
        }
    
        [CodedStep(@"Verify input BorderColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep12()
        {
            // Verify input 'BorderColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.BorderColorText.Value, app.BorderColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.BorderColor, Pages.ActivityStreamAppearance.BorderColorText.Text));
            
        }
    
        [CodedStep(@"Verify input FontColorText - portal vs db")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep13()
        {
            // Verify input 'FontColorText' value 'Exact' '000000'.
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamAppearance.FontColorText.Value, app.FontColor, ArtOfTest.Common.StringCompareType.Exact), string.Format("expected = {0} actual = {1}", app.FontColor, Pages.ActivityStreamAppearance.FontColorText.Text));
            
        }
    
        [CodedStep(@"Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'")]
        public void Appearance__Edit_Cancel_and_Save_CodedStep14()
        {
            // Verify 'Box:BorderColor' style 'Exact' '#0CB09C' on 'AlertWillAsideTag'
          //  string position;
            
            string topLeftColor = "#E5E5E5";
            string topRightColor = "#E5E5E5";
            string bottomLeftColor = "#E5E5E5";
            string bottomRightColor = "#E5E5E5";
            
            switch(app.AlertPosition)
            {
                case "topLeft": topLeftColor = "#0CB09C"; break;
                case "topRight": topRightColor = "#0CB09C"; break;
                case "bottomLeft": bottomLeftColor = "#0CB09C"; break;
                case "bottomRight": bottomRightColor = "#0CB09C"; break;
            }
            
            Pages.ActivityStreamAppearance.AlertWillAsideTag.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag0.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, topRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag1.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomLeftColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            Pages.ActivityStreamAppearance.AlertWillAsideTag2.AssertStyle().Box(ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleBox.BorderColor, bottomRightColor, ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts.HtmlStyleType.Computed, ArtOfTest.Common.StringCompareType.Same);
            
        }
    }
}
