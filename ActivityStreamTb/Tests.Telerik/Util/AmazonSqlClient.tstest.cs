using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.SqlClient;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

using System.Collections.Specialized;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
    
    public class Merchant
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string Email { get; set; }
        public string ContactNumber { get; set; }        
        public string Username { get; set; }        
        public bool IsActivated { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string StoreUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool ShowTutorial { get; set; }
        public DateTime StartDate { get; set; }
        public string PlanName { get; set; }
        public int TotalNotificationAllowed { get; set; }
        public int AlertUsage { get; set;}
        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public string FontColor { get; set; }
        public string AlertPosition { get; set; }
        
        public string Print()
        {
            string text = string.Format("Email       = {0}", this.Email);
            text = string.Format("{0} \r\nUsername   = {1}", text, this.Username);
            text = string.Format("{0} \r\nFirstName  = {1}", text, this.FirstName);
            text = string.Format("{0} \r\nLastName   = {1}", text, this.LastName);
            text = string.Format("{0} \r\nPhone      = {1}", text, this.ContactNumber);
            text = string.Format("{0} \r\nAddress 1  = {1}", text, this.Address1);
            text = string.Format("{0} \r\nAddress 2  = {1}", text, this.Address2);
            text = string.Format("{0} \r\nCity       = {1}", text, this.City);
            text = string.Format("{0} \r\nState      = {1}", text, this.State);
            text = string.Format("{0} \r\nCountry    = {1}", text, this.Country);
            text = string.Format("{0} \r\nZipCode    = {1}", text, this.ZipCode);
            text = string.Format("{0} \r\nStoreUrl   = {1}", text, this.StoreUrl);
            text = string.Format("{0} \r\nCreatedDate= {1}", text, this.CreatedDate.ToShortDateString());
            text = string.Format("{0} \r\nStartDate  = {1}", text, this.StartDate.ToShortDateString());
            text = string.Format("{0} \r\nPlan Name  = {1}", text, this.PlanName);
            text = string.Format("{0} \r\nAlert Cap  = {1}", text, this.TotalNotificationAllowed.ToString());
            text = string.Format("{0} \r\nAlert Usage= {1}", text, this.AlertUsage.ToString());        
            text = string.Format("{0} \r\nIsActivated = {1}", text, this.IsActivated.ToString());
            text = string.Format("{0} \r\nIsDeleted   = {1}", text, this.IsDeleted.ToString());
            text = string.Format("{0} \r\nShowTutorial= {1} \r\n", text, this.ShowTutorial.ToString());
            return text;
        }

        public Merchant CopyTo()
        {
            var obj = new Merchant();
            
            obj.Email = this.Email;
            obj.Username = this.Username;
            obj.FirstName = this.FirstName;
            obj.LastName = this.LastName;
            obj.ContactNumber = this.ContactNumber;
            obj.Address1 = this.Address1;
            obj.Address2 = this.Address2;
            obj.City = this.City;
            obj.State = this.State;
            obj.Country = this.Country;
            obj.ZipCode = this.ZipCode;
            obj.StoreUrl = this.StoreUrl;
            obj.CreatedDate = this.CreatedDate;
            obj.StartDate = this.StartDate;
            obj.PlanName = this.PlanName;
            obj.TotalNotificationAllowed = this.TotalNotificationAllowed;
            obj.AlertUsage = this.AlertUsage;
            obj.IsActivated = this.IsActivated;
            obj.IsDeleted = this.IsDeleted;
            obj.ShowTutorial = this.ShowTutorial;
            return obj;
        }

        public void AppendStringToProfile(string suffix)
        {
            this.Email += suffix;
            this.FirstName += suffix;
            this.LastName += suffix;
            this.ContactNumber += suffix;
            this.Address1 += suffix;
            this.Address2 += suffix;
            this.City += suffix;
            this.State += suffix;
            this.ZipCode += suffix;
            this.StoreUrl += suffix;
        }
        
        public bool CompareProfile(Merchant obj)
        {
            bool success = true;
            success &= (this.Email.Equals(obj.Email))? true: false;
            success &= (this.Username.Equals(obj.Username))? true: false;
            success &= (this.FirstName.Equals(obj.FirstName))? true: false;
            success &= (this.LastName.Equals(obj.LastName))? true: false;
            success &= (this.ContactNumber.Equals(obj.ContactNumber))? true: false;
            success &= (this.Address1.Equals(obj.Address1))? true: false;
            success &= (this.Address2.Equals(obj.Address2))? true: false;
            success &= (this.City.Equals(obj.City))? true: false;
            success &= (this.State.Equals(obj.State))? true: false;
            success &= (this.ZipCode.Equals(obj.ZipCode))? true: false;
            success &= (this.Country.Equals(obj.Country))? true: false;
            success &= (this.StoreUrl.Equals(obj.StoreUrl))? true: false;
            return success;
        }
    }

    public class AmazonSqlClient : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
       private static SqlConnection GetSqlConnection()
        {
            SqlConnection conn = new SqlConnection(
                "Server=astestinstance.c08xbhy4yjor.ap-southeast-2.rds.amazonaws.com,1433;" +
                "Database=ActivityStream;" +
                "uid=activitystream;" +
                "password=Oxygen123;");
            return conn;
        }

        private static SqlDataReader GetQuery(string query, SqlConnection conn)
        {
            SqlDataReader reader = null;
            SqlCommand cmd = new SqlCommand(query, conn);
            reader = cmd.ExecuteReader();
            return reader;
        }
        
        public static string GetAvailableEmail(string prefix)
        {
            int idx = 0;
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery(string.Format("select * from dbo.Users where Email like 'qa+{0}%'", prefix.ToLower()), conn);
            while (rdr.Read())
            {
                // get email items here
                //int num = (int) System.Convert.ChangeType(rdr["Email"].ToString().Replace(string.Format("qa+{0}", prefix).ToLower(), "").Replace("@oxygenventures.com.au", ""), typeof(int));
                int num = (int) System.Convert.ChangeType(rdr["Email"].ToString().Split('@')[0].Replace(string.Format("qa+{0}", prefix).ToLower(), ""), typeof(int));
                idx = (num > idx)? num: idx;
            }

            rdr.Close();
            conn.Close();
            
            idx += 1;
            
            return string.Format("qa+{0}{1}@oxygenventures.com.au", prefix.ToLower(), idx.ToString());
        }
        
        public static string GetLastName(string prefix)
        {
            int idx = 0;
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery(string.Format("select * from dbo.Users where Email like 'qa+{0}%'", prefix.ToLower()), conn);
            while (rdr.Read())
            {
                // get email items here
                //int num = (int) System.Convert.ChangeType(rdr["Email"].ToString().Replace(string.Format("qa+{0}", prefix).ToLower(), "").Replace("@oxygenventures.com.au", ""), typeof(int));
                int num = (int) System.Convert.ChangeType(rdr["Email"].ToString().Split('@')[0].Replace(string.Format("qa+{0}", prefix).ToLower(), ""), typeof(int));
                idx = (num > idx)? num: idx;
            }

            rdr.Close();
            conn.Close();
            
            idx += 1;
            
            return string.Format("{0} {1}", prefix, idx.ToString());
        }        
        
        public static Merchant FetchStore(string email)
        {
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            SqlDataReader rdr = GetQuery(string.Format(
                "select * " + 
                "from dbo.Subscriptions " +
                "INNER JOIN dbo.SubscriptionPackages on dbo.Subscriptions.SubscriptionPackageId = dbo.SubscriptionPackages.Id " +
                "INNER JOIN dbo.Stores on dbo.Subscriptions.StoreId = dbo.Stores.Id " +
                "INNER JOIN dbo.Users on dbo.Users.Username = dbo.Stores.StoreId " +
                "where dbo.Users.Email = '{0}'", email), conn);
            var store = new Merchant();    
            while (rdr.Read())
            {
                store.FirstName = rdr ["FirstName"].ToString();
                store.LastName = rdr["LastName"].ToString();
                store.Email = rdr["Email"].ToString();
                store.ContactNumber = rdr["ContactNumber"].ToString();
                store.Username = rdr["Username"].ToString();
                store.IsActivated = System.Convert.ToBoolean(rdr["IsActivated"].ToString());
                store.Address1 = rdr["Address1"].ToString();
                store.Country = rdr["Country"].ToString();
                store.ZipCode = rdr["ZipCode"].ToString();
                store.Address2 = rdr["Address2"].ToString();
                store.City = rdr["City"].ToString();
                store.State = rdr["State"].ToString();
                store.StoreUrl = rdr["Url"].ToString();
                store.CreatedDate = System.Convert.ToDateTime(rdr["CreatedDate"].ToString());
                store.IsDeleted = System.Convert.ToBoolean(rdr["IsDeleted"].ToString());
                store.ShowTutorial = System.Convert.ToBoolean(rdr["ShowTutorial"].ToString());
                store.StartDate = System.Convert.ToDateTime(rdr["StartDate"].ToString());
                store.PlanName = rdr["Name"].ToString();
                store.TotalNotificationAllowed = System.Convert.ToInt32(rdr["TotalNotificationAllowed"].ToString());
            }

            rdr.Close();
            conn.Close();
            
            return store;
        }
        
        public static bool ActivateStore(string email)
        {
            SqlConnection conn = GetSqlConnection();
            
            try
                
            {
                conn.Open();
                SqlDataReader rdr = GetQuery(string.Format("update dbo.Users set IsActivated=1, ShowTutorial=0 where Email='{0}'", email), conn);
                rdr.Close();
                conn.Close();
                
                return true;
            }
            catch(Exception e)
            {
                Assert.IsTrue(false, string.Format("***ERROR: Error activating store {0}\r\n{1}", email, e.Message));
                return false;
            }
        }
        
        public static string FetchInstallVerifiedEmail(string prefix, int InstallVerified)
        {
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            var list = new StringCollection();
            string query = "";
            
            if(prefix.Equals("Any"))
                query = "Email like 'qa+trial%' or Email like 'qa+cancelled%' or Email like 'qa+paid%'";
            else
                query = string.Format("Email like 'qa+{0}%'", prefix);
            
            if(InstallVerified == 1)
                query = string.Format("({0}) and dbo.Stores.InstallVerified = 1", query);
            else
                query = string.Format("({0}) and dbo.Stores.InstallVerified is null", query);
            
            SqlDataReader rdr = GetQuery(string.Format("select * FROM dbo.Stores INNER JOIN dbo.Users ON dbo.Users.Username = dbo.Stores.StoreId  where ({0}) and dbo.Users.IsActivated = 1 and dbo.Users.IsDeleted = 0", query), conn);
            
            while (rdr.Read())
            {
                list.Add(rdr["Email"].ToString());
            }

            rdr.Close();
            conn.Close();
            
            int i = Randomizer.GetRandomNumber(0, list.Count - 1);
            
            return list[i];
        }
    
        public static string FetchRandomEmail(string prefix)
        {
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            var list = new StringCollection();
            string query = string.Format("Email like 'qa+{0}%'", prefix);
            if(prefix.Equals("Any"))
                query = "Email like 'qa+trial%' or Email like 'qa+cancelled%' or Email like 'qa+paid%'";
            SqlDataReader rdr = GetQuery(string.Format("select * from dbo.Users where ({0}) and IsDeleted = 0 and IsActivated=1", query), conn);
            while (rdr.Read())
            {
                list.Add(rdr["Email"].ToString());
            }

            rdr.Close();
            conn.Close();
            
            int i = Randomizer.GetRandomNumber(0, list.Count - 1);
            
            return list[i];
        }        
        
        public static Merchant FetchStoreAppearance (string email)
        {
            SqlConnection conn = GetSqlConnection();

            conn.Open();
            var store = new Merchant();
            SqlDataReader rdr = GetQuery(string.Format("select * from dbo.StoreAppearances inner join dbo.Stores on dbo.StoreAppearances.Id = dbo.Stores.Id inner join dbo.Users on dbo.Users.Username = dbo.Stores.StoreId where dbo.Users.Email = '{0}'", email), conn);
            while (rdr.Read())
            {
                store.BackgroundColor = rdr["BackgroundColor"].ToString().Replace("#", "");
                store.BorderColor = rdr["BorderColor"].ToString().Replace("#", "");
                store.FontColor = rdr["FontColor"].ToString().Replace("#", "");
                store.AlertPosition = rdr["AlertPosition"].ToString();
            }

            rdr.Close();
            conn.Close();
            
            return store;
            
        }
        
        [CodedStep(@"Debug")]
        public void AmazonSqlClient_CodedStep()
        {
            Log.WriteLine(string.Format("email = {0}", GetAvailableEmail("Trial")));
            Log.WriteLine(string.Format("lastname = {0}", GetLastName("Trial")));
        }
    }
}
