using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class InstallationTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
        Merchant store;

        [CodedStep(@"Fetch random email")]
        public void FetchRandomEmail_CodedStep()
        {
            string email = AmazonSqlClient.FetchInstallVerifiedEmail(Data["Email"].ToString(), System.Convert.ToInt32(Data["InstallVerified"].ToString()));
            System.IO.File.AppendAllText(string.Format("C:\\Users\\Public\\{0}.txt", Data["TestName"].ToString()), "\r\n" + email);
        }
        
        [CodedStep(@"Fetch Store Details")]
        public void InstallationTest_CodedStep()
        {
            string email = FileReaderUtil.GetCurrentEmailAddress(Data["TestName"].ToString());
            store = AmazonSqlClient.FetchStore(email);
            Log.WriteLine(store.Print());
        }
    
        [CodedStep(@"Verify input 'LicenseKeyText' value.")]
        public void VerifyLicenseKeyText_CodedStep()
        {
            Assert.IsTrue(ArtOfTest.Common.CompareUtils.StringCompare(Pages.ActivityStreamInstallation.LicenseKeyText.Value, store.Username, ArtOfTest.Common.StringCompareType.Exact));
        }
    
        [CodedStep(@"Run System Check Status")]
        public void RunSystemCheck_CodedStep()
        {
            System.Threading.Thread.Sleep(5000);
            if("0".Equals(Data["InstallVerified"].ToString()))
                Pages.ActivityStreamInstallation.InstallationUnsuccessfulPTag.Wait.ForVisible();
            else
                Pages.ActivityStreamInstallation.InstallationSuccessfulPTag.Wait.ForVisible();
        }  
    }
}
