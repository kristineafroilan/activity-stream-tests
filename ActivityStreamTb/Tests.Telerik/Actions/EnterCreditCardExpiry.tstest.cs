using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class EnterCreditCardExpiry : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        [CodedStep(@"Select Credit Card Expiry")]
        public void EnterCreditCardExpiry_CodedStep()
        {
            string m = Data["CreditCardExpireMonth"].ToString();
            string y = Data["CreditCardExpireYear"].ToString();
            
            Log.WriteLine(string.Format("Selecting month {0}", m));
            Pages.ActivityStreamSubscribe.SelectMonthDiv.ScrollToVisible();
            Pages.ActivityStreamSubscribe.SelectMonthDiv.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 65, -17, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            
            var list = Pages.ActivityStreamSubscribe.SelectMonthUnorderedList.Find.AllControls<HtmlControl>();
            foreach(HtmlControl c in list)
            {
                if(c.BaseElement.InnerText.Equals(m))
                {
                    Log.WriteLine("Month InnerText: " + c.BaseElement.InnerText);
                    Log.WriteLine("Month TextContent: " + c.BaseElement.TextContent);
                    c.ScrollToVisible();
                    c.MouseHover();
                    c.MouseClick();
                    break;
                }
            }
            
            //var month = Pages.ActivityStreamSubscribe.SelectMonthUnorderedList.Find.ByContent(m);
            //Actions.ScrollToVisible(month);
            //Actions.Click(month);
            
            Log.WriteLine(string.Format("Selecting year {0}", y));
            Pages.ActivityStreamSubscribe.SelectYearDiv.ScrollToVisible();
            Pages.ActivityStreamSubscribe.SelectYearDiv.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 65, -18, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            
            list = Pages.ActivityStreamSubscribe.SelectYearUnorderedList.Find.AllControls<HtmlControl>();
            foreach(HtmlControl c in list)
            {
                if(c.BaseElement.InnerText.Equals(y))
                {
                    c.ScrollToVisible();
                    c.MouseHover();
                    c.MouseClick();
                    Log.WriteLine("Year InnerText: " + c.BaseElement.InnerText);
                    Log.WriteLine("Year TextContentText: " + c.BaseElement.TextContent);
                }
            }      
        }
    }
}
