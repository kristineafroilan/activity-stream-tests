using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace Tests_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class SubscribePaidTest : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        [CodedStep(@"Select Subscription Type")]
        public void SelectSubscriptionType_CodedStep()
        {
            string email = Data["Email"].ToString().ToLower();
            
            if("startup".Equals(email))
            {
                Pages.ActivityStreamSubscribe.Alerts200LabelTag.ScrollToVisible();
                Pages.ActivityStreamSubscribe.Alerts200LabelTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            }
            
            if("small".Equals(email))
            {
                Pages.ActivityStreamSubscribe.Alerts500LabelTag.ScrollToVisible();
                Pages.ActivityStreamSubscribe.Alerts500LabelTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            }
            
            if("medium".Equals(email))
            {
                Pages.ActivityStreamSubscribe.Alerts1kLabelTag.ScrollToVisible();
                Pages.ActivityStreamSubscribe.Alerts1kLabelTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            }
            
            if("large".Equals(email))
            {
                Pages.ActivityStreamSubscribe.Alerts10kLabelTag.ScrollToVisible();
                Pages.ActivityStreamSubscribe.Alerts10kLabelTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            }
            
            if("extreme".Equals(email))
            {
                Pages.ActivityStreamSubscribe.Alerts100kLabelTag.ScrollToVisible();
                Pages.ActivityStreamSubscribe.Alerts100kLabelTag.MouseClick(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter);
            }
        }
        
        [CodedStep(@"Verify Amount to Pay")]
        public void VerifyAmountToPay_CodedStep()
        {
            string email = Data["Email"].ToString().ToLower();
            string amount = "";
            
            switch(email)
            {
                case "startup": amount = "$9.95"; break;
                case "small": amount = "$19.95"; break;
                case "medium": amount = "$29.95"; break;
                case "large": amount = "$59.95"; break;
                case "extreme": amount = "$95.95"; break;
            }
            
            Assert.IsTrue(amount.Equals(Pages.ActivityStreamSubscribe.MonthlyAmountSpan.InnerText), string.Format("Monthly amount incorrect! [Exp]:{0} | [Act]: {1}", amount, Pages.ActivityStreamSubscribe.MonthlyAmountSpan.InnerText));
            Assert.IsTrue(amount.Equals(Pages.ActivityStreamSubscribe.TotalAmountSpan.InnerText), string.Format("Monthly amount incorrect! [Exp]:{0} | [Act]: {1}", amount, Pages.ActivityStreamSubscribe.TotalAmountSpan.InnerText));
        }
    }
}
